// vue.config.js

const FileManagerPlugin = require('filemanager-webpack-plugin')

let plugins = []

plugins.push(new FileManagerPlugin({
  onEnd: [
    {
      delete: [
        "../web/assets/*"
      ]
    },
    {
      copy: [
        { source: "./dist/js/bundle.js", destination: "../web/assets/js" },
        { source: "./dist/css/bundle.css", destination: "../web/assets/css" },
        { source: "./dist/img/*", destination: "../web/assets/img" }
      ]
    }
  ]
}))


module.exports = {
  configureWebpack: {
    plugins: plugins,
    output: {
      filename: 'js/bundle.js'
    }
  },
  css: {
    extract: {
      filename: 'css/bundle.css'
    }
  },
  chainWebpack: config => {
    config.optimization.delete('splitChunks')
  }
}