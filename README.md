Installation Guide
==========

1. Go to your console. cd to the project folder then type `composer install`
2. Define database configuration on `app/config/parameters.yml`
3. Refer to `dump.sql` to dump initial tables to your database
4. Enter `php bin/console doctrine:migrations:migrate` to update database tables
5. Enter `php bin/console fos:oauth-server:create-client --grant-type="password" --grant-type="refresh_token"` take note of `client_id` and `client_secret`
6. Rename `app/config/nelmio_cors.yml.prod.dist` to `app/config/nelmio_cors.yml` then edit it. Define clients url to `allow_origin` (Ex. `allow_origin: ['https://base-origin.com', 'https://embedded-origin.com']`)
7. Rename `app/config/services.yml.prod.dist` to `app/config/services.yml` then edit it. Define clients urls that you want to grant root folder access to `allow_root_origins` (Ex. `allow_root_origins: ['https://base-origin.com']`)
8. Create folder outside of this project and name it `uploads-prod` then `chmod -R 777` it
9. CD back to your project then grant permissions (`chmod -R 777`) to `web`, `var/cache`, `var/logs`, and `var/sessions`

And that's it!