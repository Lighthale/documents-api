FROM ubuntu:18.04

RUN apt-get update -y && apt-get install -y \
	supervisor \
	nginx \
	php7.2-fpm \
	curl \
	php-xml \
	php-zip \
	php-mbstring \
	php-mysql \
	php-curl \
	make \
	unzip

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer global require hirak/prestissimo

RUN ln -sf /dev/stdout /var/log/nginx/access.log && \
	ln -sf /dev/stdout /var/log/nginx/error.log

COPY ./dockerfiles/nginx.conf /etc/nginx/sites-enabled/default
COPY ./dockerfiles/www.conf /etc/php/7.2/fpm/pool.d/www.conf
COPY ./dockerfiles/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY ./dockerfiles/php.ini /etc/php/7.2/fpm/conf.d/custom.php.ini
RUN  mkdir /run/php && touch /run/php/php7.2-fpm.sock

WORKDIR /code
COPY . /code

ENV database_host=documents-api-db \
	database_port=3306 \
	database_name=application \
	database_user=root \
	database_password=password \
	mailer_transport=smtp \
	mailer_host=smtp.mandrillapp.com \
	mailer_port=2525 \
	mailer_user=info@voodoocreative.com.au \
	mailer_password= \
	mailer_from_name='SaffronRobo' \
	mailer_from_email='no-reply@saffronrobo.com' \
	secret=e023ccd354d759b9c869ef930ebb654e8f2a794a \
	crm_url=http://localhost:8080 \
	crm_api_url=http://localhost:8081 \
	biz_api_url=http://localhost:8085 \
	documents_url=http://localhost:8082 \
	documents_api_url=http://localhost:8083

CMD ["/usr/bin/supervisord"]
