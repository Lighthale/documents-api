<?php
namespace AppBundle\Service;

use AppBundle\Entity\DocumentViewer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class SearchService
{

    protected $table = 'document_view';
    protected $result;
    protected $em;


    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * Get document name by keyword
     * @param Request $request
     * @return array
     */
    public function autoComplete(Request $request)
    {
        $keyword = $request->query->get('keyword');
        if(strlen($keyword) > 0){
            $statement = $this->em->getConnection()->prepare("SELECT name as keyword FROM `{$this->getTableName()}` WHERE name like '%{$keyword}%'");
            $statement->execute();

           $this->result = $statement->fetchAll();
        }

        return $this->result;
    }

    /**
     * Get document record by keyword
     * @param Request $request
     * @return array
     */
    public function getRecordByParam(Request $request)
    {
        $keyword = $request->query->get('keyword');
        if(strlen($keyword) > 0){
            $statement = $this->em->getConnection()->prepare("SELECT * FROM `{$this->getTableName()}` WHERE name like '%{$keyword}%'");
            $statement->execute();

            $query = $statement->fetchAll();

            foreach ($query as $item) {
                $this->result[] = [
                    'id'                => $item['id'],
                    'type'              => $item['type'],
                    'variant'           => $item['variant'],
                    'name'              => $item['name'],
                    'directory_id'      => $item['directory_id'],
                    'server_file_name'  => $item['server_file_name'],
                    'user' => [
                            'id'           => (int) $item['user_id'],
                            'username'          => $item['username'],
                        ],
                    'extension'         => $item['extension'],
                    'size'              => (int) $item['size'],
                    'created_at'        => $item['created_at'],
                    'date_modified'     => $item['date_modified'],
                ];
            }
        }

        return $this->result;
    }

    /**
     * @return string
     */
    private function getTableName()
    {
        return $this->em->getClassMetadata('AppBundle:DocumentViewer')->getTableName();
    }
}