<?php

namespace AppBundle\Service;


use AppBundle\Entity\Directory;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\Integer;
use phpDocumentor\Reflection\Types\Nullable;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class DirectoryManagerService
{
    # Entity Manager
    private $em;

    # Container
    private $container;

    # Serializer Service
    private $serializerService;

    # TokenStorage Service to get current logged in user
    private $tokenStorage;

    # Validator Service
    private $validatorService;

    # Audit Trail Service
    private $auditTrailService;

    /**
     * DirectoryManagerService constructor.
     * @param EntityManagerInterface $em
     * @param ContainerInterface $container
     * @param SerializerService $serializerService
     * @param TokenStorageInterface $tokenStorage
     * @param ValidatorService $validatorService
     * @param AuditTrailService $auditTrailService
     */
    public function __construct(
        EntityManagerInterface $em, ContainerInterface $container,
        SerializerService $serializerService, TokenStorageInterface $tokenStorage,
        ValidatorService $validatorService, AuditTrailService $auditTrailService
    )
    {
        $this->em = $em;
        $this->container = $container;
        $this->serializerService = $serializerService;
        $this->tokenStorage = $tokenStorage;
        $this->validatorService = $validatorService;
        $this->auditTrailService = $auditTrailService;
    }

    /**
     * @param Request $request
     * @return array|mixed
     */
    public function getRootDirectory(Request $request)
    {
        $directoryRepo = $this->em->getRepository('AppBundle:Directory');

        # Embedded root directory
        if (!$this->isOriginHasAccessToRoot($request)) {
            $requestReferer = $this->overrideReferer($request);
            $refererDir = $this->createRefererDirectories($requestReferer);
            $serializedDirectory = $this->serializerService->serializeObject($refererDir);

            # Hide keys
            unset($serializedDirectory['user']);
            unset($serializedDirectory['variant']);
            unset($serializedDirectory['parent']);

            return $serializedDirectory;
        }

        # Not embedded root directory
        $directories = $directoryRepo->findBy(['parent' => null], ['createdAt' => 'DESC']);
        $data = [];
        $data['child'] = [];
        $data['files'] = [];

        foreach ($directories as $directory) {
            $serializedDirectory = $this->serializerService->serializeObject($directory);
            unset($serializedDirectory['child']);
            unset($serializedDirectory['files']);
            $data['child'][] = $serializedDirectory;
        }

        $filesRepo = $this->em->getRepository('AppBundle:File');
        $files = $filesRepo->findBy(['directory' => null], ['createdAt' => 'DESC']);

        foreach ($files as $file) {
            $serializedFile = $this->serializerService->serializeObject($file);
            $data['files'][] = $serializedFile;
        }

        if ($this->isOriginHasAccessToRoot($request)) {
            $data['id'] = 0;
            $data['name'] = 'root';
        }

        return $data;
    }

    /**
     * @param Request $request
     * @param Directory|null $directory
     * @return mixed
     */
    public function getDirectory(Request $request, Directory $directory = null)
    {
        $this->isOriginAllowedToAccessDirectory($request, $directory);

        if (!$directory)
            throw new NotFoundHttpException('Directory not found.');

        $rootDir = null;

        # Generate breadcrumbs based on origin access
        if (!$this->isOriginHasAccessToRoot($request)) {
            $referer = $this->overrideReferer($request);
            $trimmedReferer = $this->trimRequestOrigin($referer);

            # Get root directory
            $rootDir = $this->getRefererRootDirectory($trimmedReferer);
        }

        $serializedDirectory = $this->serializerService->serializeObject($directory);
        $serializedDirectory['breadcrumbs'] = $this->generateBreadcrumbs($directory, $rootDir);

        return $serializedDirectory;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function addNewDirectory(Request $request)
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $contents = json_decode($request->getContent(), true);

        $constraint = new Assert\Collection([
            'parent_id' => null,
            'name' => new Assert\NotBlank()
        ]);
        $contents = $this->validatorService->validate($contents, $constraint);

        $directory = new Directory();
        $directory->setUser($user);
        $directory->setName($contents['name']);

        if ($contents['parent_id']) {
            $directoryRepo = $this->em->getRepository('AppBundle:Directory');
            $parentDirectory = $directoryRepo->findOneBy([
                'id' => $contents['parent_id']
            ]);

            if (!$parentDirectory)
                throw new NotFoundHttpException('Parent directory not found.');

            $this->isOriginAllowedToAccessDirectory($request, $parentDirectory);
            $directory->setParent($parentDirectory);
            $directory = $this->renameDirectoryNameIfExist($user, $directory, $parentDirectory);
        } else {
            if (!$this->isOriginHasAccessToRoot($request))
                throw new AccessDeniedException('Access denied.');

            $directory = $this->renameDirectoryNameIfExist($user, $directory);
        }

        $basePath = $this->container->getParameter('file_base_directory');
        $directoryPath = $basePath . '/' . $this->getDirectoryPath($directory);
        $fileSystem = new Filesystem();

        if(!$fileSystem->exists($directoryPath))
            $fileSystem->mkdir($directoryPath);

        $this->em->persist($directory);
        $this->em->flush();

        # Audit trail action
        $remarks = '{"directory_name": "' . $directory->getName() . '"}';
        $this->auditTrailService->addAuditTrail($this->auditTrailService::ACTION_CREATE_DIRECTORY, $directory->getParent(), $directory, null, $remarks);

        $serializedDirectory = $this->serializerService->serializeObject($directory);
        return $serializedDirectory;
    }

    /**
     * @param Request $request
     * @param null $directoryId
     * @return null|object
     */
    public function updateDirectory(Request $request, $directoryId = null)
    {
        $contents = json_decode($request->getContent(), true);

        # Validate inputs
        $constraint = new Assert\Collection([
            'name' => new Assert\NotBlank()
        ]);
        $contents = $this->validatorService->validate($contents, $constraint);

        $directoryRepo = $this->em->getRepository('AppBundle:Directory');
        $directory = $directoryRepo->findOneBy([
            'id' => $directoryId
        ]);

        # Checks if directory exist
        if (!$directory)
            throw new NotFoundHttpException('Directory not found.');

        $this->isOriginAllowedToAccessDirectory($request, $directory, true);

        $subDirectories = $directoryRepo->findByParent($directory->getParent());

        # Checks if directory name has duplicate
        foreach ($subDirectories as $subDirectory) {
            if ($subDirectory->getName() === $contents['name'] && $subDirectory->getId() != $directory->getId())
                throw new ConflictHttpException('Destination already has a directory named ' . $contents['name']);
        }

        $basePath = $this->container->getParameter('file_base_directory');
        $oldDirectoryPath = $basePath . '/' . $this->getDirectoryPath($directory);

        $oldDirectoryName = $directory->getName();
        $directory->setName($contents['name']);
        $this->em->persist($directory);

        $newDirectoryPath = $basePath . '/' . $this->getDirectoryPath($directory);
        $fileSystem = new Filesystem();

        if($fileSystem->exists($oldDirectoryPath))
            rename($oldDirectoryPath, $newDirectoryPath);

        $this->em->flush();

        # Audit Trail
        $remarks = '{"old_filename": "' . $oldDirectoryName . '", "new_filename": "' . $directory->getName() . '"}';
        $this->auditTrailService->addAuditTrail($this->auditTrailService::ACTION_RENAME_DIRECTORY, $directory->getParent(), $directory, null, $remarks);

        $serializedDirectory = $this->serializerService->serializeObject($directory);
        return $serializedDirectory;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function deleteDirectories(Request $request)
    {
        $contents = json_decode($request->getContent(), true);

        # Validate inputs
        $constraint = new Assert\Collection([
            'ids' => new Assert\NotBlank()
        ]);
        $contents = $this->validatorService->validate($contents, $constraint);

        $directories = $this->em
            ->createQuery('SELECT d FROM AppBundle\Entity\Directory d WHERE d.id IN(:ids)')
            ->setParameter('ids', $contents['ids'])
            ->execute()
        ;

        if (!$directories)
            throw new NotFoundHttpException('Directory(ies) not found.');

        # Gets filesystem service
        $fileSystem = new Filesystem();

        # Gets the upload base path
        $basePath = $this->container->getParameter('file_base_directory');

        foreach ($directories as $directory)
            $this->isOriginAllowedToAccessDirectory($request, $directory, true);

        foreach ($directories as $directory) {
            # Audit trail action
            $remarks = '{"directory_id": "' . $directory->getId() . '", "directory_name": "' . $directory->getName() . '"}';
            $this->auditTrailService->addAuditTrail($this->auditTrailService::ACTION_DELETE_DIRECTORY, $directory->getParent(), null,null, $remarks);

            $this->em->remove($directory);

            $directoryPath = $basePath . '/' . $this->getDirectoryPath($directory);

            # Deletes directory in server
            if ($fileSystem->exists($directoryPath))
                $fileSystem->remove($directoryPath);
        }

        $this->em->flush();

        return true;
    }

    /**
     * Returns the url string of the directory
     *
     * @param Directory|null $directory
     * @param string $path
     * @return bool|null|string
     */
    public function getDirectoryPath(Directory $directory = null, $path = "")
    {
        if (!$directory)
            return null;

        $parent = $directory->getParent();

        if ($parent) {
            $directoryRepo = $this->em->getRepository('AppBundle:Directory');
            $parent = $directoryRepo->findOneById($parent->getId());
            $path = $directory->getName() . '/' . $path;
            return $this->getDirectoryPath($parent, $path);
        }

        $path = $directory->getName() . '/' . $path;

        return substr(str_replace(' ', '_', $path), 0, -1);
    }

    /**
     * @param Directory $directory
     * @param Directory|null $rootDirectory
     * @param array $breadcrumbs
     * @return array
     */
    public function generateBreadcrumbs(Directory $directory, Directory $rootDirectory = null, $breadcrumbs = [])
    {
        $breadcrumbs[] = [
            'id' => $directory->getId(),
            'label' => $directory->getName()
        ];

        if ($directory === $rootDirectory)
            return array_reverse($breadcrumbs);
        elseif (!$directory->getParent())
            $breadcrumbs[] = ['id' => 0, 'label' => 'root'];
        else
            return $this->generateBreadcrumbs($directory->getParent(), $rootDirectory, $breadcrumbs);

        return array_reverse($breadcrumbs);
    }

    /**
     * @param Directory $childDirectory
     * @param Directory $parentDirectory
     * @return bool
     */
    public function isInParentDirectory(Directory $childDirectory, Directory $parentDirectory)
    {
        # childDirectory's parent is parentDirectory (returns true)
        if ($childDirectory->getParent() == $parentDirectory || $childDirectory === $parentDirectory)
            return true;

        # If getParent reaches null then it means that childDirectory is not in parentDirectory (returns false)
        if (!$childDirectory->getParent())
            return false;

        # Recursive call childDirectory (up one level)
        return $this->isInParentDirectory($childDirectory->getParent(), $parentDirectory);
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function isOriginHasAccessToRoot(Request $request)
    {	
        $requestOrigin = $request->headers->get('origin');	
        $allowedRootOrigins = $this->container->getParameter('allow_root_origins');

        # Access root directory when allowed or request from share link -> it has no origin
        if (in_array($requestOrigin, $allowedRootOrigins) ||  empty($requestOrigin))
	   	    return true;

	    return false;
    }

    /**
     * @param Request $request
     * @param Directory $directory
     * @param bool $includeRoot
     * @return bool
     */
    public function isOriginAllowedToAccessDirectory(Request $request, Directory $directory, $includeRoot = false)
    {
 	
        if ($this->isOriginHasAccessToRoot($request))
            return true;

        # Get referer / override referer
        $trimmedReferer = $this->trimRequestOrigin($this->overrideReferer($request));

        # Get referer's root directory
        $rootDir = $this->getRefererRootDirectory($trimmedReferer);

        # Denied if trying to access the directory outside of object's root || if $includeRoot is true then deny access to origin's root (used to avoid renaming and deleting referer's root folder)
        if (!$this->isInParentDirectory($directory, $rootDir) || ($includeRoot && $directory === $rootDir)) {
            throw new AccessDeniedException('Access Denied!');
        }

        return true;
    }

    /**
     * @param Request $request
     * @param null $directoryId
     * @return mixed
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getDirectoryAuditTrailLogs(Request $request, $directoryId = null)
    {
        $directoryRepo = $this->em->getRepository('AppBundle:Directory');
        $mainDirectory = $directoryRepo->findOneById($directoryId);

        if ($directoryId <= 0) {
            # Check if origin has access to root directory
            if (!$this->isOriginHasAccessToRoot($request))
                throw new AccessDeniedException('Access denied.');
        } else {
            if (!$mainDirectory)
                throw new NotFoundHttpException('Directory not found.');

            # Checks if origin has access to directory
            $this->isOriginAllowedToAccessDirectory($request, $mainDirectory);
        }

        $limit = ($request->get('max_results') != null) ? $request->get('max_results') : 30;
        $offset = ($request->get('offset')) ? $request->get('offset') : 0;
        $startDate = ($request->get('start_date')) ? (new \DateTime($request->get('start_date')))->format('Y-m-d H:i:s') : null;
        $endDate = ($request->get('end_date')) ? (new \DateTime($request->get('end_date')))->format('Y-m-d H:i:s') : null;
        $logs = $this->auditTrailService->getAuditTrailLogs($directoryId, $directoryId, null, $limit, $offset, $startDate, $endDate);

        $result['id'] = 0;
        $result['name'] = 'root';
        $result['created_at'] = null;
        $result['updated_at'] = null;
        $result['total_count'] = $logs['count'];
        $result['logs'] = $logs['logs'];

        if ($mainDirectory) {
            $result['id'] = $mainDirectory->getId();
            $result['name'] = $mainDirectory->getName();
            $result['created_at'] = $mainDirectory->getCreatedAt()->format('Y-m-d H:i:s');
            $result['updated_at'] = $mainDirectory->getUpdatedAt()->format('Y-m-d H:i:s');
        }

        return $result;
    }

    ############## PRIVATE FUNCTIONS

    /**
     * @param null $referer
     * @return mixed
     */
    private function getRefererRootDirectory($referer = null)
    {
        if (!$referer)
            throw new NotFoundHttpException('getRefererRootDirectory: No referer defined');

        $refererDirInfo = explode('/', $referer);
        $directoryRepo = $this->em->getRepository('AppBundle:Directory');
        $originDirectory = $directoryRepo->findOneBy([
            'name' => $refererDirInfo[0],
            'parent' => null
        ]);

        if (!$originDirectory)
            throw new NotFoundHttpException('Origin directory not found.');

        # Recursive tracing of directories
        unset($refererDirInfo[0]);
        $isDirFound = false;
        foreach ($refererDirInfo as $refererDir) {
            $childDirs = $originDirectory->getChild();

            foreach ($childDirs as $childDir) {
                if ($childDir->getName() == $refererDir) {
                    $originDirectory = $childDir;
                    $isDirFound = true;
                    break;
                }
            }

            if ($isDirFound) {
                $isDirFound = false;
            } else {
                throw new NotFoundHttpException('Cannot find directory ' . $refererDir . '.');
            }
        }

        return $originDirectory;
    }

    /**
     * @param User $user
     * @param Directory $directory
     * @param Directory|null $parentDirectory
     * @return Directory
     */
    private function renameDirectoryNameIfExist(User $user, Directory $directory, Directory $parentDirectory = null)
    {
        $childDirectories = null;
        $directoryRepo = $this->em->getRepository('AppBundle:Directory');

        if ($parentDirectory)
            $childDirectories = $parentDirectory->getChild();
        else
            $childDirectories = $directoryRepo->findBy(['parent' => null]);

        foreach ($childDirectories as $childDirectory) {
            if ($childDirectory->getName() === $directory->getName()) {
                $directory->setName($directory->getName() . ' Copy');
                return $this->renameDirectoryNameIfExist($user, $directory, $parentDirectory);
            }
        }

        return $directory;
    }

    /**
     * @param null $requestOrigin
     * @return mixed
     */
    private function trimRequestOrigin($requestOrigin = null)
    {
        return str_replace(['http://', 'https://'], '', $requestOrigin);
    }


    /**
     * @param null $requestReferer
     * @return Directory|null|object
     */
    private function createRefererDirectories($requestReferer = null)
    {
        if (!$requestReferer)
            throw new NotFoundHttpException('Referer not found');

        $trimmedRequestReferer = $this->trimRequestOrigin($requestReferer);
        $refererDirInfo =  explode('/', $trimmedRequestReferer);

        $rootDir = $this->generateDirectoriesByReferer($refererDirInfo);

        return $rootDir;
    }

    /**
     * @param array $refererDirInfo
     * @param int $index
     * @param Directory|null $parentDir
     * @return Directory|null|object
     */
    private function generateDirectoriesByReferer($refererDirInfo = [], $index = 0, Directory $parentDir = null)
    {
        if (!sizeof($refererDirInfo))
            throw new NotFoundHttpException('$refererDirInfo in generateDirectoriesByReferer must have a value.');

        $directoryRepo = $this->em->getRepository('AppBundle:Directory');
        $directory = $directoryRepo->findOneBy([
            'name' => $refererDirInfo[$index],
            'parent' => $parentDir
        ]);

        if (!$directory) {
            # Create origin directory since it doesn't exist.
            $directory = new Directory();
            $directory->setUser($this->tokenStorage->getToken()->getUser());
            $directory->setName($refererDirInfo[$index]);

            if ($parentDir)
                $directory->setParent($parentDir);

            # Create physical folder in server
            $fileSystem = new Filesystem();
            $basePath = $this->container->getParameter('file_base_directory');
            $directoryPath = $basePath . '/' . $this->getDirectoryPath($directory);

            if(!$fileSystem->exists($directoryPath))
                $fileSystem->mkdir($directoryPath);

            # Save directory details into database
            $this->em->persist($directory);
            $this->em->flush();
        }

        $index++;
        if ($index < sizeof($refererDirInfo))
            return $this->generateDirectoriesByReferer($refererDirInfo, $index, $directory);

        return $directory;
    }

    /**
     * @param Request $request
     * @return mixed|null|string|string[]
     */
    private function overrideReferer(Request $request)
    {
        if ($request->get('override'))
            return $request->get('override');

        return $request->headers->get('referer');
    }
}