<?php

namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ValidatorService
{
    /**
     * @param array $contents
     * @param array $constraints
     * @return array
     */
    public function validate($contents = [], $constraints = [])
    {
        # Validate fields
        $validator = Validation::createValidator();
        $violations = $validator->validate($contents, $constraints);

        if (sizeof($violations)) {
            $messages = null;
            foreach ($violations as $violation)
                $messages .= $violation->getPropertyPath() . ' ' . $violation->getMessage();
            throw new BadRequestHttpException($messages);
        }

        return $contents;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function validateEmbeddedParameters(Request $request)
    {
        # Validate embedded fields
        $contents = [];
        $constraintCollections = [
            'object_type' => new Assert\Choice(['contact', 'organization', 'opportunity', 'conversation']),
            'object_id' => new Assert\Type(['type' => 'integer'])
        ];

        if ($request->get('object_type')) {
            $contents['object_type'] = $request->get('object_type');

            if ($contents['object_type'] == 'opportunity') {
                $constraintCollections['stage_id'] = new Assert\Type(['type' => 'integer']);

                if ($request->get('stage_id'))
                    $contents['stage_id'] = ((int)$request->get('stage_id')) ? (int)$request->get('stage_id') : $request->get('stage_id');
            }
        }

        if ($request->get('object_id'))
            $contents['object_id'] = ((int)$request->get('object_id')) ? (int)$request->get('object_id') : $request->get('object_id');

        $constraint = new Assert\Collection($constraintCollections);
        $contents = $this->validate($contents, $constraint);

        return $contents;
    }
}