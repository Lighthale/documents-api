<?php

namespace AppBundle\Service;

use AppBundle\Entity\AuditTrail;
use AppBundle\Entity\Directory;
use AppBundle\Entity\File;
use AppBundle\Entity\FileMetadata;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class SerializerService
{
    private $encoders;

    private $normalizer;

    private $baseAttributes;

    public function __construct()
    {
        $this->baseAttributes = [
            'usernameCanonical', 'salt', 'password', 'emailCanonical', 'roles',
            'accountNonExpired', 'accountNonLocked', 'credentialsNonExpired', 'enabled', 'superAdmin',
            'groups', 'groupNames', '__isInitialized__', 'createdAt', 'updatedAt', 'plainPassword',
            'lastLogin', 'confirmationToken', 'passwordRequestedAt', '__initializer__', '__cloner__'
        ];

        $this->encoders = [new JsonEncoder()];
        $this->normalizer = new ObjectNormalizer(null, new CamelCaseToSnakeCaseNameConverter());
        $this->normalizer->setCircularReferenceLimit(2);
        $this->normalizer->setCircularReferenceHandler(function ($obj) {
            return $obj->getId();
        });
    }

    /**
     * @param $object
     * @return mixed
     */
    public function serializeObject($object = null)
    {
        if ($object instanceof AuditTrail)
            unset($this->baseAttributes[array_search('createdAt', $this->baseAttributes)]);

        $this->normalizer->setIgnoredAttributes($this->baseAttributes);
        $normalizers = [$this->normalizer];
        $serializer = new Serializer($normalizers, $this->encoders);
        $serializedObject = json_decode($serializer->serialize($object, 'json'), true);

        if ($object instanceof Directory)
            $serializedObject =  $this->cleanSerializedDirectory($serializedObject);

        if ($object instanceof File)
            $serializedObject = $this->cleanSerializedFile($serializedObject);

        if ($object instanceof FileMetadata)
            $serializedObject = $this->cleanSerializedFileMetadata($serializedObject);

        return $serializedObject;
    }

    /**
     * @param array $serializedDirectory
     * @return array
     */
    private function cleanSerializedDirectory($serializedDirectory = [])
    {
        $serializedDirectory['variant'] = 'directory';
        unset($serializedDirectory['parent']['parent']);
        unset($serializedDirectory['parent']['child']);
        unset($serializedDirectory['parent']['files']);

        foreach ($serializedDirectory['child'] as $index => $value) {
            unset($serializedDirectory['child'][$index]['parent']);
            unset($serializedDirectory['child'][$index]['child']);
            unset($serializedDirectory['child'][$index]['files']);
            $serializedDirectory['child'][$index]['variant'] = 'directory';
        }

        foreach ($serializedDirectory['files'] as $index => $value) {
            unset($serializedDirectory['files'][$index]['directory']['user']);
            unset($serializedDirectory['files'][$index]['directory']['parent']);
            unset($serializedDirectory['files'][$index]['directory']['child']);
            unset($serializedDirectory['files'][$index]['directory']['files']);
            unset($serializedDirectory['files'][$index]['metadata']);
            unset($serializedDirectory['files'][$index]['versions']);
            $serializedDirectory['files'][$index]['variant'] = 'file';
        }

        return $serializedDirectory;
    }

    /**
     * @param array $serializedFile
     * @return array
     */
    private function cleanSerializedFile($serializedFile = [])
    {
        $serializedFile['variant'] = 'file';

        if ($serializedFile['directory']) {
            $serializedFile['directory']['variant'] = 'directory';
            unset($serializedFile['directory']['parent']);
            unset($serializedFile['directory']['child']);
            unset($serializedFile['directory']['files']);
        }

        if ($serializedFile['metadata']) {
            $serializedFile['metadata']['variant'] = 'metadata';
            unset($serializedFile['metadata']['file']);
        }

        if ($serializedFile['versions']) {
            foreach ($serializedFile['versions'] as $index => $value) {
                $serializedFile['versions'][$index]['variant'] = 'version';
                unset($serializedFile['versions'][$index]['file']);
            }
        }

        return $serializedFile;
    }

    /**
     * @param array $serializedMetadata
     * @return array
     */
    private function cleanSerializedFileMetadata($serializedMetadata = [])
    {
        $serializedMetadata['variant'] = 'metadata';
        $serializedMetadata['file']['variant'] = 'file';
        $serializedMetadata['file']['directory']['variant'] = 'directory';
        unset($serializedMetadata['file']['directory']['parent']);
        unset($serializedMetadata['file']['directory']['child']);
        unset($serializedMetadata['file']['directory']['files']);

        foreach($serializedMetadata['file']['versions'] as $index => $value) {
            unset($serializedMetadata['file']['versions'][$index]['file']);
            unset($serializedMetadata['file']['versions'][$index]['directory']);
        }

        unset($serializedMetadata['file']['metadata']);

        return $serializedMetadata;
    }
}