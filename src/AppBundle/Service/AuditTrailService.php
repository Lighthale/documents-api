<?php

namespace AppBundle\Service;


use AppBundle\Entity\AuditTrail;
use AppBundle\Entity\Directory;
use AppBundle\Entity\File;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class AuditTrailService
{
    const ACTION_CREATE_DIRECTORY = 'create directory';
    const ACTION_UPLOAD_FILE = 'upload file';
    const ACTION_DELETE_DIRECTORY = 'delete directory';
    const ACTION_DELETE_FILE = 'delete file';
    const ACTION_RENAME_DIRECTORY = 'rename directory';
    const ACTION_RENAME_FILE = 'rename file';
    const ACTION_DOWNLOAD_DIRECTORY = 'download directory';
    const ACTION_DOWNLOAD_FILE = 'download file';
    const ACTION_VERSION_FILE = 'version file';
    const ACTION_DOWNLOAD_FILE_VERSION = 'download file version';
    const ACTION_DELETE_FILE_VERSION = 'delete file version';
    const ACTION_FILE_METADATA = 'file metadata';

    # Entity Manager
    private $em;

    # Container
    private $container;

    # Serializer Service
    private $serializerService;

    # TokenStorage Service to get current logged in user
    private $tokenStorage;

    public function __construct(EntityManagerInterface $em, ContainerInterface $container, TokenStorageInterface $tokenStorage)
    {
        $this->em = $em;
        $this->container = $container;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param null $action
     * @param Directory $parentDirectory
     * @param Directory|null $directory
     * @param File|null $file
     * @param null $remarks
     */
    public function addAuditTrail($action = null, Directory $parentDirectory = null, Directory $directory = null, File $file = null, $remarks = null)
    {
        $auditTrail = new AuditTrail();
        $auditTrail->setUser($this->tokenStorage->getToken()->getUser());
        $auditTrail->setAction($action);
        $auditTrail->setParent($parentDirectory);
        $auditTrail->setDirectory($directory);
        $auditTrail->setFile($file);
        $auditTrail->setRemarks($remarks);
        $this->em->persist($auditTrail);
        $this->em->flush();
    }

    /**
     * @param null $parentId
     * @param null $directoryId
     * @param null $fileId
     * @param int $limit
     * @param int $offset
     * @param null $startDate
     * @param null $endDate
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getAuditTrailLogs($parentId = null, $directoryId = null, $fileId = null, $limit = 30, $offset = 0, $startDate = null, $endDate = null)
    {
        $conn = $this->em->getConnection();
        $sql = 'SELECT * FROM view_audit_trails';

        if ($parentId !== null && $parentId > 0)
            $sql .= ' WHERE parent_id = ' . $parentId;
        elseif ($parentId !== null && $parentId == 0)
            $sql .= ' WHERE parent_id IS NULL';

        if ($directoryId && $directoryId > 0)
            $sql .= ' OR directory_id = ' . $directoryId;

        if ($fileId && $fileId > 0)
            $sql .= ' AND file_id = ' . $fileId;

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $datas = $stmt->fetchAll();
        $results['count'] = sizeof($datas);

        if ($startDate && $endDate)
            $sql .= ' AND created_at BETWEEN ' . $startDate . ' AND ' . $endDate;

        $sql .= ' ORDER BY created_at DESC';

        if ($limit)
            $sql .= ' LIMIT ' . $limit;

        if ($limit && $offset)
            $sql .= ' OFFSET ' . $offset;

      $stmt = $conn->prepare($sql);
        $stmt->execute();
        $datas = $stmt->fetchAll();
        $logDate = null;
        $logDates = [];
        $logs = [];

        foreach ($datas as $index => $data) {
            $data['remarks'] = json_decode($data['remarks'], true);
            $date = (new \DateTime($data['created_at']))->format('Y-m-d');
            $data['time'] = (new \DateTime($data['created_at']))->format('H:i:s');
            unset($data['created_at']);

            if ($logDate != $date) {
                $logDate = $date;
                $logDates[] = $date;
                $logs[] = [
                    'date' => $date,
                    'activities' => [
                        $data
                    ]
                ];
            } else {
                $logs[array_search($logDate, $logDates)]['activities'][] = $data;
            }
        }

        $results['logs'] = $logs;

        return $results;
    }
}