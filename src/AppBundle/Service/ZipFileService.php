<?php

namespace AppBundle\Service;


class ZipFileService
{
    /**
     * @param string $zipName
     * @param array $directories
     * @param array $files
     * @return \ZipArchive
     */
    public function createZipFile($zipName = 'TemporaryZipFile.zip', $directories = [], $files = [])
    {
        $zip = new \ZipArchive();

        # Creates and opens a zip file and store to server temporarily
        $zip->open($zipName,  \ZipArchive::CREATE);

        if (sizeof($directories)) {
            # Store directories into zip
            foreach($directories as $directory){
                $rootPath = realpath($directory['path']);
                $iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($directory['path']), \RecursiveIteratorIterator::LEAVES_ONLY);

                foreach ($iterator as $index => $item)
                {
                    # Skip directories (they would be added automatically)
                    if (!$item->isDir())
                    {
                        # Get real and relative path for current file
                        $filePath = $item->getRealPath();
                        $relativePath = substr($filePath, strlen($rootPath) + 1);

                        try {
                            # Adds current file in a folder to zip
                            $zip->addFile($filePath, $directory['name'] . '/' . $relativePath);
                        } catch (ContextErrorException $e) {
                            throw new NotFoundHttpException('Something\'s wrong with the directory ' . $directory['name'] . '.');
                        }
                    }
                }
            }
        }

        if (sizeof($files)) {
            # Store files into zip
            foreach ($files as $file) {
                try {
                    $zip->addFromString(basename($file['path']), file_get_contents($file['path']));
                    $zip->renameName(basename($file['path']), $file['name']);
                } catch (ContextErrorException $e) {
                    throw new NotFoundHttpException('Couldn\'t find the file ' . basename($file['path']) . '.');
                }
            }
        }

        # Closes zip
        $zip->close();

        return $zip;
    }
}