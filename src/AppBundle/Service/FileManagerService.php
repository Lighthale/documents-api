<?php

namespace AppBundle\Service;

use AppBundle\Entity\File;
use AppBundle\Entity\FileMetadata;
use AppBundle\Entity\FileVersions;
use Doctrine\Bundle\DoctrineCacheBundle\DependencyInjection\Definition\FileSystemDefinition;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class FileManagerService
{
    # Base64 Service
    private $base64Service;

    # Directory Manager Service
    private $directoryManagerService;

    # Entity Manager
    private $em;

    # Container Interface to call parameters
    private $container;

    # TokenStorage Service to get current logged in user
    private $tokenStorage;

    # Serializer Service
    private $serializerService;

    # Zip File Service
    private $zipFileService;

    # Validator Service
    private $validatorService;

    # Audit Trail Service
    private $auditTrailService;

    # Email Service
    private $emailAlertService;

    /**
     * FileManagerService constructor.
     * @param Base64Service $base64Service
     * @param DirectoryManagerService $directoryManagerService
     * @param EntityManagerInterface $em
     * @param ContainerInterface $container
     * @param TokenStorageInterface $tokenStorage
     * @param SerializerService $serializerService
     * @param ZipFileService $zipFileService
     * @param ValidatorService $validatorService
     * @param AuditTrailService $auditTrailService
     * @param EmailAlertService $emailAlertService
     */
    public function __construct(
        Base64Service $base64Service, DirectoryManagerService $directoryManagerService,
        EntityManagerInterface $em, ContainerInterface $container,
        TokenStorageInterface $tokenStorage, SerializerService $serializerService,
        ZipFileService $zipFileService, ValidatorService $validatorService,
        AuditTrailService $auditTrailService, EmailAlertService $emailAlertService
    )
    {
        $this->base64Service = $base64Service;
        $this->directoryManagerService = $directoryManagerService;
        $this->em = $em;
        $this->container = $container;
        $this->tokenStorage = $tokenStorage;
        $this->serializerService = $serializerService;
        $this->zipFileService = $zipFileService;
        $this->validatorService = $validatorService;
        $this->auditTrailService = $auditTrailService;
        $this->emailAlertService = $emailAlertService;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function addFile(Request $request)
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $contents = json_decode($request->getContent(), true);

        # Validate fields
        $constraint = new Assert\Collection([
            'name'          => new Assert\NotBlank(),
            'directory'     => null,
            'type'          => new Assert\NotBlank(),
            'size'          => new Assert\Type(['type' => 'integer']),
            'extension'     => new Assert\NotBlank(),
            'date_modified' => new Assert\NotBlank(),
            'file'          => new Assert\NotBlank(),
        ]);
        $contents = $this->validatorService->validate($contents, $constraint);
        $directory = null;
        $directoryPath = null;

        # Checks if origin has access
        $this->isOriginHasAccessToDirectory($request, $contents['directory']);

        if ($contents['directory']) {
            # Checks if directory exist
            $directoryRepo = $this->em->getRepository('AppBundle:Directory');
            $directory = $directoryRepo->findOneById($contents['directory']);

            if (!$directory)
                throw new NotFoundHttpException('Directory not exist.');

            $directoryPath = $this->directoryManagerService->getDirectoryPath($directory);
        }

        $file = $this->versionFileIfExist($contents['name'], $contents['extension'], $contents['directory'], $contents['size'], $contents['date_modified']);

        if ($file)
            $fileName = $file->getName();
        else
            $fileName = $contents['name'];

        # Uploads file
        $base64 = $contents['file'];

        if ($directoryPath) {
            $fileDirectory = $this->container->getParameter('file_base_directory') . '/' . $directoryPath;
            $fileUrlPath = $request->getSchemeAndHttpHost() . '/' . $directoryPath;
        } else {
            $fileDirectory = $this->container->getParameter('file_base_directory');
            $fileUrlPath = $request->getSchemeAndHttpHost();
        }

        $filePrefix = $this->container->getParameter('file_prefix');
        $uploadedFile = $this->base64Service->convertToFile($base64, $fileDirectory, $fileName, $filePrefix, $contents['extension']);
        $fileUrlPath .= '/' . $uploadedFile['name'];

        if (!$file) {
            # Add file details to database
            $file = new File();
            $file->setUser($user);
            $file->setName($fileName);
            $file->setDirectory($directory);
            $file->setServerFileName($uploadedFile['name']);
            $file->setExtension($contents['extension']);
            $file->setPath($fileUrlPath);
            $file->setType($contents['type']);
            $file->setSize($contents['size']);
            $file->setDateModified(new \DateTime($contents['date_modified']));
            $this->em->persist($file);
            $this->em->flush();

            # Audit trail action
            $remarks = '{"file_name": "' . $file->getName() . '"}';
            $this->auditTrailService->addAuditTrail($this->auditTrailService::ACTION_UPLOAD_FILE, $file->getDirectory(), null, $file, $remarks);
        }

        $serializedFile = $this->serializerService->serializeObject($file);
        return $serializedFile;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function deleteFiles(Request $request)
    {
        # Gets filesystem service
        $fileSystem = new Filesystem();

        # Gets the upload base path
        $basePath = $this->container->getParameter('file_base_directory');

        # Gets the request contents
        $contents = json_decode($request->getContent(), true);

        # Validate inputs
        $constraint = new Assert\Collection([
            'directory_ids' => null,
            'file_ids' => null
        ]);
        $contents = $this->validatorService->validate($contents, $constraint);

        # Checks if no values in directories and files
        if (!sizeof($contents['directory_ids']) && !sizeof($contents['file_ids']))
            throw new NotFoundHttpException('Nothing to delete.');

        # Check origin directory access
        if (sizeof($contents['directory_ids'])) {
            foreach ($contents['directory_ids'] as $directoryId) {
                $this->isOriginHasAccessToDirectory($request, $directoryId);
            }
        }

        # Check origin file access
        $files = null;
        if (sizeof($contents['file_ids'])) {
            $files = $this->em
                ->createQuery('SELECT f FROM AppBundle\Entity\File f WHERE f.id IN(:ids)')
                ->setParameter('ids', $contents['file_ids'])
                ->execute()
            ;

            if (!$files)
                throw new NotFoundHttpException('File(s) not found.');

            foreach ($files as $file) {
                $fileDirectoryId = ($file->getDirectory()) ? $file->getDirectory()->getId() : 0;
                $this->isOriginHasAccessToDirectory($request, $fileDirectoryId);
            }
        }

        # Delete files (delete files first before deleting directories to avoid file not found error)
        if ($files) {
            foreach ($files as $file) {
                $fileDirectory = $file->getDirectory();

                # Audit trail action
                $remarks = '{"file_id": "' . $file->getId() . '", "file_name": "' . $file->getName() . '"}';
                $this->auditTrailService->addAuditTrail($this->auditTrailService::ACTION_DELETE_FILE, $fileDirectory, null, null, $remarks);
                $filePath = $basePath . '/' . $this->getFilePath($file);

                # Deletes the file in server
                if ($fileSystem->exists($filePath))
                    $fileSystem->remove($filePath);

                # Deletes file record
                $this->em->remove($file);
            }
        }

        # Delete directories
        if (sizeof($contents['directory_ids'])) {
            $directories = $this->em
                ->createQuery('SELECT d FROM AppBundle\Entity\Directory d WHERE d.id IN(:ids)')
                ->setParameter('ids', $contents['directory_ids'])
                ->execute()
            ;

            if (!$directories)
                throw new NotFoundHttpException('Directory not found.');

            foreach ($directories as $directory) {
                # Audit trail action
                $remarks = '{"directory_id": "' . $directory->getId() . '", "directory_name": "' . $directory->getName() . '"}';
                $this->auditTrailService->addAuditTrail($this->auditTrailService::ACTION_DELETE_DIRECTORY, $directory->getParent(), null, null, $remarks);

                $directoryPath = $basePath . '/' . $this->directoryManagerService->getDirectoryPath($directory);

                # Deletes directory in server
                if ($fileSystem->exists($directoryPath))
                    $fileSystem->remove($directoryPath);

                # Deletes directory record
                $this->em->remove($directory);
            }
        }

        $this->em->flush();

        return true;
    }

    /**
     * @param Request $request
     * @return bool|mixed
     */
    public function downloadFiles(Request $request)
    {
        $fileIds = json_decode($request->get('file_ids'), true);
        $dirIds = json_decode($request->get('directory_ids'), true);

        if (!is_array($fileIds))
            throw new BadRequestHttpException('[files]: Key is required or the input type is invalid.');

        if (!is_array($dirIds))
            throw new BadRequestHttpException('[directories]: Key is required or the input type is invalid.');

        if (!sizeof($fileIds) && !sizeof($dirIds))
            throw new NotFoundHttpException('Nothing to download.');

        $fileBaseDir = $this->container->getParameter('file_base_directory');

        $directories = [];
        $files = [];

        # Check origin directory access
        if (sizeof($dirIds)) {
            foreach ($dirIds as $directoryId) {
                $this->isOriginHasAccessToDirectory($request, $directoryId);
            }
        }

        # Check origin file access
        $fileObjs = null;
        if (sizeof($fileIds)) {
            $fileObjs = $this->em
                ->createQuery('SELECT f FROM AppBundle\Entity\File f WHERE f.id IN(:ids)')
                ->setParameter('ids', $fileIds)
                ->execute()
            ;

            if (!$fileObjs)
                throw new NotFoundHttpException('File(s) not found.');

            foreach ($fileObjs as $file) {
                $fileDirectoryId = ($file->getDirectory()) ? $file->getDirectory()->getId() : 0;
                $this->isOriginHasAccessToDirectory($request, $fileDirectoryId);
            }
        }

        if (sizeof($dirIds)) {
            # Collecting directories for zip
            $dirsCollection = $this->em
                ->createQuery('SELECT d FROM AppBundle\Entity\Directory d WHERE d.id IN(:ids)')
                ->setParameter('ids', $dirIds)
                ->execute()
            ;

            if (!$dirsCollection)
                throw new NotFoundHttpException('No directories found.');

            foreach ($dirsCollection as $dirObject) {
                $directories[] = [
                    'path' => $fileBaseDir . '/' . $this->directoryManagerService->getDirectoryPath($dirObject),
                    'name' => str_replace(' ', '_', $dirObject->getName())
                ];

                # Audit trail action
                $remarks = '{"directory_id": "' . $dirObject->getId() . '", "directory_name": "' . $dirObject->getName() . '"}';
                $this->auditTrailService->addAuditTrail($this->auditTrailService::ACTION_DOWNLOAD_DIRECTORY, $dirObject->getParent(), $dirObject, null, $remarks);
            }
            # -- Collecting directories for zip
        }

        if (sizeof($fileIds)) {
            # Collecting files for zip
            $filesCollection = $this->em
                ->createQuery('SELECT f FROM AppBundle\Entity\File f WHERE f.id IN(:ids)')
                ->setParameter('ids', $fileIds)
                ->execute()
            ;

            if (!$filesCollection)
                throw new NotFoundHttpException('No file found.');

            foreach ($filesCollection as $fileObject) {
                $files[] = [
                    'path' => $fileBaseDir . '/' . $this->getFilePath($fileObject),
                    'name' => $fileObject->getName()
                ];

                # Audit trail action
                $remarks = '{"file_id": "' . $fileObject->getId() . '", "file_name": "' . $fileObject->getName() . '"}';
                $this->auditTrailService->addAuditTrail($this->auditTrailService::ACTION_DOWNLOAD_FILE, $fileObject->getDirectory(), null, $fileObject, $remarks);
            }
            # -- Collecting files for zip
        }

        # Returns file path for single file download
        if (!sizeof($directories) && sizeof($files) == 1)
            return $files[0];

        # Put folders and files into zip
        $zipPrefix = $this->container->getParameter('zip_prefix');
        $zipName =  '/tmp/'. $zipPrefix . time() . ".zip";

        $zip = $this->zipFileService->createZipFile($zipName, $directories, $files);

        # Set response headers to trigger download
        header('Content-Type', 'application/zip');
        header('Content-disposition: attachment; filename="' . $zipPrefix . time() . '.zip"');
        header('Content-Length: ' . filesize($zipName));

        # Triggers download
        readfile($zipName);

        # Deletes the temporary zip file
        unlink($zipName);

        return false;
    }

    /**
     * @param Request $request
     * @param null|File $file
     * @return mixed
     */
    public function getFile(Request $request, File $file = null)
    {
        if (!$file)
            throw new NotFoundHttpException('File not found.');

        $directoryId = ($file->getDirectory()) ? $file->getDirectory()->getId() : 0;
        # Checks if origin has access
        $this->isOriginHasAccessToDirectory($request, $directoryId);

        $serializedFile = $this->serializerService->serializeObject($file);
        return $serializedFile;
    }

    /**
     * @param Request $request
     * @param File|null $file
     * @return mixed
     */
    public function renameFile(Request $request, File $file = null)
    {
        $contents = json_decode($request->getContent(), true);
        # Validate fields
        $constraint = new Assert\Collection([
            'name' => new Assert\NotBlank()
        ]);
        $contents = $this->validatorService->validate($contents, $constraint);

        if (!$file)
            throw new NotFoundHttpException('File not found.');

        $directoryId = ($file->getDirectory()) ? $file->getDirectory()->getId() : 0;
        # Checks if origin has access
        $this->isOriginHasAccessToDirectory($request, $directoryId);
        $fileRepo = $this->em->getRepository('AppBundle:File');
        $duplicateFile = $fileRepo->findOneBy([
            'name' => $contents['name'],
            'extension' => $file->getExtension(),
            'directory' => $file->getDirectory()
        ]);

        if ($duplicateFile && $duplicateFile->getId() != $file->getId())
            throw new ConflictHttpException('Filename already exist.');

        $basePath = $this->container->getParameter('file_base_directory');
        $oldFilePath = $basePath . '/' . $this->getFilePath($file);
        $oldFileName = $file->getName();
        $file->setName($contents['name']);
        $file->setServerFileName(str_replace(' ', '_', $contents['name']));
        $this->em->persist($file);
        $newFilePath = $basePath . '/' . $this->getFilePath($file);
        $newFileName = $file->getName();
        # Rename the physical file
        $fileSystem = new Filesystem();

        if($fileSystem->exists($oldFilePath))
            rename($oldFilePath, $newFilePath);

        $this->em->flush();
        # Audit trail action
        $remarks = '{"old_file_name": "' . $oldFileName . '", "new_file_name": "' . $newFileName . '"}';
        $this->auditTrailService->addAuditTrail($this->auditTrailService::ACTION_RENAME_FILE, $file->getDirectory(), null, $file, $remarks);
        $serializedFile = $this->serializerService->serializeObject($file);

        return $serializedFile;
    }

    /**
     * @param Request $request
     * @param File|null $file
     * @return mixed
     */
    public function updateFileMetaData(Request $request, File $file = null)
    {
        if (!$file)
            throw new NotFoundHttpException('File not found.');

        $directoryId = ($file->getDirectory()) ? $file->getDirectory()->getId() : 0;
        # Checks if origin has access
        $this->isOriginHasAccessToDirectory($request, $directoryId);
        $contents = json_decode($request->getContent(), true);
        # Validate fields
        $constraint = new Assert\Collection([
            'doc_created_date' => null,
            'valid_from' => null,
            'valid_to' => new Assert\Date(),
            'alerts' => null,
            'alert_on' => null,
            'recipients' => null
        ]);
        $contents = $this->validatorService->validate($contents, $constraint);
        $alertOnOptions = [FileMetadata::ONE_DAY_BEFORE, FileMetadata::ONE_WEEK_BEFORE, FileMetadata::THREE_DAYS_BEFORE];

        if (!in_array($contents['alert_on'], $alertOnOptions))
            throw new NotFoundHttpException('Invalid [alert_on] choice.');

        if ($contents['recipients']) {
            foreach ($contents['recipients'] as $recipient) {
                # Validate recipients emails
                $email['recipients'] = $recipient;
                $constraint = new Assert\Collection([
                    'recipients' => new Assert\Email()
                ]);
                $email = $this->validatorService->validate($email, $constraint);
            }
        }

        $fileMetaDataRepo = $this->em->getRepository('AppBundle:FileMetadata');
        $fileMetaData = $fileMetaDataRepo->findOneByFile($file);

        if (!$fileMetaData) {
            $fileMetaData = $this->addFileMetadata($contents, $file);
        } else {
            if ($contents['doc_created_date'])
                $fileMetaData->setDocCreatedDate(new \DateTime($contents['doc_created_date']));

            if ($contents['valid_from'])
                $fileMetaData->setValidFrom(new \DateTime($contents['valid_from']));

            $fileMetaData->setValidTo(new \DateTime($contents['valid_to']));

            if ($contents['alerts']) {
                $fileMetaData->setAlerts($contents['alerts']);

                if ($contents['alert_on'])
                    $fileMetaData->setAlertOn($contents['alert_on']);
                else
                    throw new NotFoundHttpException('[alert_on] is required whenever [alerts] was set to true.');
            }

            if ($contents['recipients'])
                $fileMetaData->setRecipients(json_encode($contents['recipients']));

            $this->em->persist($fileMetaData);
            $this->em->flush();
        }

        # Audit trail action
        $remarks = '{"file_name": "' . $file->getName() . '"}';
        $this->auditTrailService->addAuditTrail($this->auditTrailService::ACTION_FILE_METADATA, $file->getDirectory(), null, $file, $remarks);
        $serializedFileMetadata = $this->serializerService->serializeObject($fileMetaData);

        return $serializedFileMetadata;
    }

    /**
     * @param File $file
     * @return null|string
     */
    public function getFilePath(File $file)
    {
        $filePath = null;
        $fileDirectory = $file->getDirectory();

        if ($fileDirectory)
            $filePath = $this->directoryManagerService->getDirectoryPath($fileDirectory) . '/' . $file->getServerFileName();
        else
            $filePath = $file->getServerFileName();

        return $filePath;
    }

    /**
     * @param Request $request
     * @param File|null $file
     * @param FileVersions|null $version
     * @return array
     */
    public function downloadFileArchivePath(Request $request, File $file = null, FileVersions $version = null)
    {
        if (!$file || !$version)
            throw new NotFoundHttpException('File not found.');

        $directoryId = ($file->getDirectory()) ? $file->getDirectory()->getId() : 0;
        # Checks if origin has access
        $this->isOriginHasAccessToDirectory($request, $directoryId);

        $fileVersionRepo = $this->em->getRepository('AppBundle:FileVersions');
        $fileVersion = $fileVersionRepo->findOneBy([
            'file' => $file,
            'id' => $version
        ]);

        if (!$fileVersion)
            throw new NotFoundHttpException('File not found');

        $archivePath = $this->container->getParameter('file_base_directory_archive');
        $directoryPath = $this->directoryManagerService->getDirectoryPath($fileVersion->getFile()->getDirectory());
        $filePath = $archivePath . '/' . $directoryPath . '/' . $fileVersion->getServerFileName();

        # Audit trail action
        $remarks = '{"version_id": "' . $fileVersion->getId() . '", "file_name": "' . $fileVersion->getFile()->getName() . '"}';
        $this->auditTrailService->addAuditTrail($this->auditTrailService::ACTION_DOWNLOAD_FILE_VERSION, $fileVersion->getFile()->getDirectory(), null, $fileVersion->getFile(), $remarks);

        return [
            'path' => $filePath,
            'file_name' => $fileVersion->getFile()->getName()
        ];
    }

    /**
     * @param Request $request
     * @param File|null $file
     * @param FileVersions|null $version
     */
    public function deleteFileVersion(Request $request, File $file = null, FileVersions $version = null)
    {
        if (!$file || !$version)
            throw new NotFoundHttpException('File not found.');

        $directoryId = ($file->getDirectory()) ? $file->getDirectory()->getId() : 0;
        # Checks if origin has access
        $this->isOriginHasAccessToDirectory($request, $directoryId);

        $fileVersionRepo = $this->em->getRepository('AppBundle:FileVersions');
        $fileVersion = $fileVersionRepo->findOneBy([
            'id' => $version,
            'file' => $file
        ]);

        if (!$fileVersion)
            throw new NotFoundHttpException('File version not found.');

        $archivePath = $this->container->getParameter('file_base_directory_archive');
        $directoryPath = $this->directoryManagerService->getDirectoryPath($fileVersion->getFile()->getDirectory());
        $filePath = $archivePath . '/' . $directoryPath . '/' . $fileVersion->getServerFileName();

        $fileSystem = new Filesystem();

        # Deletes the file in server
        if ($fileSystem->exists($filePath))
            $fileSystem->remove($filePath);

        # Audit trail action
        $remarks = '{"version_id": "' . $fileVersion->getId() . '", "file_name": "' . $fileVersion->getFile()->getName() . '"}';
        $this->auditTrailService->addAuditTrail($this->auditTrailService::ACTION_DELETE_FILE_VERSION, $fileVersion->getFile()->getDirectory(), null, $fileVersion->getFile(), $remarks);

        $this->em->remove($fileVersion);
        $this->em->flush();
    }


    /**
     * @param Request $request
     * @param File null $file
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getFileAuditTrailLogs(Request $request, File $file = null)
    {
        if (!$file)
            throw new NotFoundHttpException('File not found.');

        $directoryId = ($file->getDirectory()) ? $file->getDirectory()->getId() : 0;
        # Checks if origin has access
        $this->isOriginHasAccessToDirectory($request, $directoryId);

        if (!$file)
            throw new NotFoundHttpException('File not found.');

        $limit = ($request->get('max_results') != null) ? $request->get('max_results') : 30;
        $offset = ($request->get('offset')) ? $request->get('offset') : 0;
        $startDate = ($request->get('start_date')) ? (new \DateTime($request->get('start_date')))->format('Y-m-d H:i:s') : null;
        $endDate = ($request->get('end_date')) ? (new \DateTime($request->get('end_date')))->format('Y-m-d H:i:s') : null;

        $logs = $this->auditTrailService->getAuditTrailLogs($directoryId, null, $file->getId(), $limit, $offset, $startDate, $endDate);

        $result['id'] = $file->getId();
        $result['name'] = $file->getName();
        $result['created_at'] = $file->getCreatedAt()->format('Y-m-d H:i:s');
        $result['updated_at'] = $file->getUpdatedAt()->format('Y-m-d H:i:s');
        $result['count'] = $logs['count'];
        $result['logs'] = $logs['logs'];

        return $result;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getFileAlertList(Request $request)
    {
        $this->isOriginHasAccessToDirectory($request, 0);

        $fileMetadataRepo = $this->em->getRepository('AppBundle:FileMetadata');
        $fileMetadatas = $fileMetadataRepo->findByAlerts(1);

        $serializedFileMetadata = [];
        foreach ($fileMetadatas as $fileMetadata) {
            $temp = $this->serializerService->serializeObject($fileMetadata);
            unset($temp['name']);
            unset($temp['details']);
            unset($temp['status']);
            unset($temp['version']);
            unset($temp['category']);
            unset($temp['confidential']);
            $serializedFileMetadata[] = $temp;
        }

        return $serializedFileMetadata;
    }

    /**
     * @param Request $request
     */
    public function emailFiles(Request $request)
    {
        $fileIds = json_decode($request->get('file_ids'), true);
        $recipients = json_decode($request->get('recipients'), true);

        if (!is_array($fileIds))
            throw new BadRequestHttpException('[files]: Key is required or the input type is invalid.');

        if (!is_array($recipients))
            throw new BadRequestHttpException('[recipients]: Key is required or the input type is invalid.');

        if (!sizeof($fileIds))
            throw new NotFoundHttpException('No files to send.');

        if (!sizeof($fileIds))
            throw new NotFoundHttpException('No recipients defined.');

        # Check origin file access
        $fileObjs = null;
        $fileNames = [];
        if (sizeof($fileIds)) {
            $fileObjs = $this->em
                ->createQuery('SELECT f FROM AppBundle\Entity\File f WHERE f.id IN(:ids)')
                ->setParameter('ids', $fileIds)
                ->execute()
            ;

            if (!$fileObjs)
                throw new NotFoundHttpException('File(s) not found.');

            foreach ($fileObjs as $file) {
                $fileDirectoryId = ($file->getDirectory()) ? $file->getDirectory()->getId() : 0;
                $fileNames[] = $file->getName();
                $this->isOriginHasAccessToDirectory($request, $fileDirectoryId);
            }
        }

        $shareUrl = $this->container->getParameter('share_doc_url') . '?ids=' .$request->get('file_ids') . '&action=download';
        $status = $this->emailAlertService->sendEmailWithDocs($recipients, $shareUrl, $fileNames);

        if (!$status)
            throw new NotFoundHttpException('Failed to send email');
    }

    #========================== PRIVATE FUNCTIONS

    /**
     * @param null $fileName
     * @param null $extension
     * @param null $directory
     * @param null $size
     * @return bool|null|object
     */
    private function versionFileIfExist($fileName = null, $extension = null, $directory = null, $size = null, $dateModified = null)
    {
        $directory = ($directory) ? $directory : null; # sets to null if the value is 0;
        $fileRepo = $this->em->getRepository('AppBundle:File');
        $duplicateFile = $fileRepo->findOneBy([
            'name' => [$fileName, str_replace(' ', '_' , $fileName)],
            'extension' => $extension,
            'directory' => $directory
        ]);

        # Version if file exists
        if ($duplicateFile) {
            $encryptedFileName = md5($duplicateFile->getName() . date("Y-m-d H:i:s"));

            # Create version record
            $fileVersion = new FileVersions();
            $fileVersion->setFileId($duplicateFile);
            $fileVersion->setUser($duplicateFile->getUser());
            $fileVersion->setServerFileName($encryptedFileName . '.' . $duplicateFile->getExtension());
            $fileVersion->setSize($duplicateFile->getSize());
            $fileVersion->setUploadedAt($duplicateFile->getUpdatedAt());
            $this->em->persist($fileVersion);

            # Archive physical file
            $fileSystem = new Filesystem();
            $filePath = $this->container->getParameter('file_base_directory') . '/' . $this->getFilePath($duplicateFile);
            $archiveFilePath = $this->container->getParameter('file_base_directory_archive') . '/' . $this->getFilePath($duplicateFile);
            $renameFilePath = $this->container->getParameter('file_base_directory_archive') . '/' . $this->directoryManagerService->getDirectoryPath($duplicateFile->getDirectory()) . '/' . $encryptedFileName . '.' . $duplicateFile->getExtension();

            if ($fileSystem->exists($filePath)) {
                $fileSystem->copy($filePath, $archiveFilePath);
                $fileSystem->remove($filePath);
            }

            if ($fileSystem->exists($archiveFilePath))
                rename($archiveFilePath, $renameFilePath);

            # Updates current file record
            $duplicateFile->setUser($this->tokenStorage->getToken()->getUser());
            $duplicateFile->setSize($size);
            $duplicateFile->setDateModified(new \DateTime($dateModified));
            $this->em->persist($duplicateFile);
            $this->em->flush();

            # Audit trail action
            $remarks = '{"version_id": "' . $fileVersion->getId() . '","file_name": "' . $fileName . '.' . $extension . '"}';
            $this->auditTrailService->addAuditTrail($this->auditTrailService::ACTION_VERSION_FILE, $duplicateFile->getDirectory(), null, $duplicateFile, $remarks);

            return $duplicateFile;
        }

        # No existing file.
        return false;
    }

    /**
     * @param array $contents
     * @param File|null $file
     * @return mixed
     */
    private function addFileMetadata($contents = [], File $file = null)
    {
        if (!$file)
            throw new NotFoundHttpException('File not found.');

        $fileMetaDataRepo = $this->em->getRepository('AppBundle:FileMetadata');
        $fileMetaDataEntity = $fileMetaDataRepo->findOneByFile($file);

        if ($fileMetaDataEntity)
            throw new ConflictHttpException('File metadata already exist.');

        $fileMetaData = new FileMetadata();
        $fileMetaData->setFile($file);

        if ($contents['doc_created_date'])
            $fileMetaData->setDocCreatedDate(new \DateTime($contents['doc_created_date']));

        if ($contents['valid_from'])
            $fileMetaData->setValidFrom(new \DateTime($contents['valid_from']));

        if ($contents['valid_to'])
            $fileMetaData->setValidTo(new \DateTime($contents['valid_to']));

        if ($contents['alerts']) {
            $fileMetaData->setAlerts($contents['alerts']);
            $fileMetaData->setAlertOn($contents['alert_on']);
        }

        if ($contents['recipients'])
            $fileMetaData->setRecipients(json_encode($contents['recipients']));

        $this->em->persist($fileMetaData);
        $this->em->flush();
        return $fileMetaData;
    }

    /**
     * @param Request $request
     * @param null $directoryId
     */
    private function isOriginHasAccessToDirectory(Request $request, $directoryId = null)
    {
        $directoryRepo = $this->em->getRepository('AppBundle:Directory');
        $mainDirectory = $directoryRepo->findOneById($directoryId);

        if ($mainDirectory) {
            # Checks if origin has access to directory
            $this->directoryManagerService->isOriginAllowedToAccessDirectory($request, $mainDirectory);
        } else {
            # Check if origin has access to root directory
            if (!$this->directoryManagerService->isOriginHasAccessToRoot($request))
                throw new AccessDeniedException('Access denied.');
        }
    }
}
