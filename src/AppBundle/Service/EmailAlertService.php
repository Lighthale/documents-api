<?php

namespace AppBundle\Service;

use AppBundle\Entity\FileMetadata;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;

class EmailAlertService
{
    const EXPIRE_TEMPLATE = 'expire-template';
    const SEND_DOCS_EMAIL_TEMPLATE = 'send-docs-email-template';

    # Expire template
    private $expireTemplate;

    # Send docs template
    private $sendDocsEmailTemplate;

    # Entity Manager
    private $em;

    # Container
    private $container;

    public function __construct(EntityManagerInterface $em, ContainerInterface $container)
    {
        $this->em = $em;
        $this->container = $container;

        # Creating Templates
        $this->expireTemplate =
            '
            Hi %s,
            
            Your document(s) %s will expire in %s.
            
            Thanks,
            
            Saffron Admin
            ';

        $this->sendDocsEmailTemplate =
            '
            Hi %s,
            
            %s has shared you these following documents %s. Click the link below to download.
            
            %s
            
            Thanks!,
            
            Saffron Admin
            ';
    }

    /**
     * @param null $templateType
     * @param array $inputs
     * @return string
     */
    public function generateTemplate($templateType = null, $inputs = [])
    {
        switch($templateType){
            case EmailAlertService::EXPIRE_TEMPLATE:
                $template = sprintf($this->expireTemplate, $inputs[0], $inputs[1], $inputs[2]);
                break;
            case EmailAlertService::SEND_DOCS_EMAIL_TEMPLATE:
                $template = sprintf($this->sendDocsEmailTemplate, $inputs[0], $inputs[1], $inputs[2], $inputs[3]);
                break;
            default:
                $template = sprintf($this->expireTemplate, $inputs[0], $inputs[1], $inputs[2]);
                break;
        }

        return $template;
    }

    /**
     * @param string $subject
     * @param string $message
     * @param array $recipients
     * @return mixed
     */
    public function sendMail($subject = 'Document Management Mail', $message = 'No template used', $recipients = [])
    {
        if (!sizeof($recipients))
            throw new NotFoundHttpException('No recepients defined.');

        $data = [
            'subject' => $subject,
            'message' => $message,
            'recipients' => $recipients
        ];

        $data_string = json_encode($data);

        $ch = curl_init('http://dev-api.saffronrobo.com/emails/send');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string)
            ]
        );

        $result = curl_exec($ch);
        $response = json_decode($result, true);

        return $response;
    }

    /**
     * @return mixed
     */
    private function getExpiringDocsList()
    {
        $rsm = new ResultSetMapping();

        $fileMetadata = $this->em
            ->createQuery('
                SELECT m FROM AppBundle\Entity\FileMetadata m
                WHERE m.alerts = :alerts
                AND (
                    (m.alertOn = :one_day_before AND m.validTo = :one_day)
                    OR (m.alertOn = :three_days_before AND m.validTo = :three_days)
                    OR (m.alertOn = :one_week_before AND m.validTo = :seven_days)
                )
            
            ', $rsm)
            ->setParameter('alerts', true)
            ->setParameter('one_day_before', 'one-day-before')
            ->setParameter('one_day', (new \DateTime('+1 day'))->format('Y-m-d 00:00:00'))
            ->setParameter('three_days_before', 'three-days-before')
            ->setParameter('three_days', (new \DateTime('+3 day'))->format('Y-m-d 00:00:00'))
            ->setParameter('one_week_before', 'one-week-before')
            ->setParameter('seven_days', (new \DateTime('+7 day'))->format('Y-m-d 00:00:00'))
            ->execute();

        return $fileMetadata;
    }

    /**
     * @return bool
     */
    public function sendExpiringDocsAlerts()
    {
        $fileMetadatas = $this->getExpiringDocsList();
//        $recipients = $this->container->getParameter('alert_recipients');

        if (!$fileMetadatas)
            echo "[" . (new \DateTime())->format('Y-m-d H:i:s') . "]: No expiring documents.\n";

        foreach ($fileMetadatas as $metadata) {
            $file = $metadata->getFile();
            $recipients = $metadata->getRecipients();

            switch($metadata->getAlertOn()){
                case FileMetadata::ONE_DAY_BEFORE:
                    $days = '1 day';
                    break;
                case FileMetadata::THREE_DAYS_BEFORE:
                    $days = '3 days';
                    break;
                case FileMetadata::ONE_WEEK_BEFORE:
                    $days = '7 days';
                    break;
                default:
                    $days = 'X days';
                    break;
            }

            $template = $this->generateTemplate(EmailAlertService::EXPIRE_TEMPLATE, ['user', $file->getName(), $days]);

            $status = $this->sendMail('Your document will expire!', $template, $recipients);

            if ($status['code'] = 200)
                echo "[" . (new \DateTime())->format('Y-m-d H:i:s') . "]: Email sent to . " . implode(", ", $recipients) . " | Document Name: " . $file->getName() . " | Days left: " . $days . "\n";
            else
                echo "[" . (new \DateTime())->format('Y-m-d H:i:s') . "]: Failed to send to recipients " . implode(", ", $recipients) . " | File: " . $file->getName() . " | Days left: " . $days . "\n";
        }

        return true;
    }

    /**
     * @param array $recipients
     * @param null $shareUrl
     * @param array $fileNames
     * @return bool
     */
    public function sendEmailWithDocs($recipients = [], $shareUrl = null, $fileNames = []) {
        $template = $this->generateTemplate(EmailAlertService::SEND_DOCS_EMAIL_TEMPLATE, ['user', 'admin', implode(', ', $fileNames), $shareUrl]);
        $status = $this->sendMail('Downloaded documents', $template, $recipients);

        if ($status['code'] = 200)
            return true;
        else
            return false;
    }
}