<?php

namespace AppBundle\Service;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeExtensionGuesser;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;

/**
 * @author Dawid Góra <dawidgora@icloud.com>
 */
class Base64Service
{

    /** @var string */
    private $filePrefix;

    /**
     * @param string $base64
     * @param string $targetPath
     * @param string $filePrefix
     * @param null $fileName
     * @param null $fileExtension
     * @return array
     */
    public function convertToFile(string $base64, string $targetPath, $fileName = null, $filePrefix = null, $fileExtension = null)
    {
        $this->filePrefix = $filePrefix;

        if (!$fileName)
            $fileName = $this->generateFileName();

        $fileName = str_replace(' ', '_', $fileName);
        $filePath = $this->generateFilePath($targetPath, $fileName);

        $fileSystem = new Filesystem();
        if(!$fileSystem->exists($targetPath))
            $fileSystem->mkdir($targetPath);

        $file = fopen($filePath, 'wb');
        $data = explode(',', $base64);
        fwrite($file, base64_decode($data[1], true));
        fclose($file);
        rename($filePath, $filePath);

        return [
            'name' => $fileName,
            'extension' => $fileExtension
        ];
    }

    /**
     * @param string $targetPath
     * @param string $fileName
     * @return string
     */
    private function generateFilePath(string $targetPath, string $fileName)
    {
        return $targetPath . '/' . $fileName;
    }

    /**
     * @return string
     */
    private function generateFileName(): string
    {
        return uniqid($this->filePrefix, true);
    }

    /**
     * @param string $filePath
     * @return string
     */
    private function getFileExt(string $filePath)
    {
        $guesser = MimeTypeGuesser::getInstance();
        $extensionGuesser = new MimeTypeExtensionGuesser();

        return $extensionGuesser->guess(
            $guesser->guess($filePath)
        );
    }



}