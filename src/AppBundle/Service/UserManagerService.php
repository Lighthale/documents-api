<?php

namespace AppBundle\Service;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class UserManagerService
{
    # Entity Manager
    private $em;

    # Container
    private $container;

    # Serializer Service
    private $serializerService;

    # TokenStorage Service to get current logged in user
    private $tokenStorage;

    # Validator Service
    private $validatorService;

    /**
     * UserManagerService constructor.
     * @param EntityManagerInterface $em
     * @param ContainerInterface $container
     * @param SerializerService $serializerService
     * @param TokenStorageInterface $tokenStorage
     * @param ValidatorService $validatorService
     */
    public function __construct(
        EntityManagerInterface $em, ContainerInterface $container,
        SerializerService $serializerService, TokenStorageInterface $tokenStorage,
        ValidatorService $validatorService
    )
    {
        $this->em = $em;
        $this->container = $container;
        $this->serializerService = $serializerService;
        $this->tokenStorage = $tokenStorage;
        $this->validatorService = $validatorService;
    }

    /**
     * @return array
     */
    public function getUsers()
    {
        $userRepo = $this->em->getRepository('AppBundle:User');
        $users = $userRepo->findBy([], ['id' => 'ASC']);

        $data = [];
        foreach ($users as $user) {
            $serializedUser = $this->serializerService->serializeObject($user);
            $data[] = $serializedUser;
        }

        return $data;
    }

    /**
     * @param User|null $user
     * @return mixed
     */
    public function getUserDetails(User $user = null)
    {
        if (!$user)
            throw new NotFoundHttpException('User not found');

        $serializedUser = $this->serializerService->serializeObject($user);

        return $serializedUser;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function addUser(Request $request)
    {
        $contents = json_decode($request->getContent(), true);

        # Validate fields
        $constraint = new Assert\Collection([
            'email'     => new Assert\Email(),
            'password'  => new Assert\NotBlank(),
        ]);
        $contents = $this->validatorService->validate($contents, $constraint);

        $userManager = $this->container->get('fos_user.user_manager');
        $emailExist = $userManager->findUserByEmail($contents['email']);

        # Check if the user exists to prevent Integrity constraint violation error in the insertion
        if($emailExist)
            throw new ConflictHttpException('User/Email already exist');

        $user = $userManager->createUser();
        $user->setUsername($contents['email']);
        $user->setEmail($contents['email']);
        $user->setEmailCanonical($contents['email']);
        $user->setEnabled(1);
        $user->setPlainPassword($contents['password']);
        $userManager->updateUser($user);

        $serializedUser = $this->serializerService->serializeObject($user);
        return $serializedUser;
    }

    /**
     * @param Request $request
     * @param User|null $user
     * @return mixed
     */
    public function updateUser(Request $request, User $user = null)
    {
        if (!$user)
            throw new NotFoundHttpException('User not found');

        if ($user->getUsername() === 'admin')
            throw new AccessDeniedException('Access denied');

        $contents = json_decode($request->getContent(), true);

        # Validate fields
        $constraint = new Assert\Collection([
            'email'     => new Assert\Email(),
            'password'  => new Assert\NotBlank(),
        ]);
        $contents = $this->validatorService->validate($contents, $constraint);

        $userManager = $this->container->get('fos_user.user_manager');
        $emailExist = $userManager->findUserByEmail($contents['email']);

        if($emailExist && $emailExist->getId() != $user->getId())
            throw new ConflictHttpException('User/Email already exist');

        $user->setUsername($contents['email']);
        $user->setEmail($contents['email']);
        $user->setEmailCanonical($contents['email']);
        $user->setPlainPassword($contents['password']);
        $userManager->updateUser($user);

        $serializedUser = $this->serializerService->serializeObject($user);
        return $serializedUser;
    }

    /**
     * @param User|null $user
     * @return bool
     */
    public function deleteUser(User $user = null)
    {
        if (!$user)
            throw new NotFoundHttpException('User not found');

        if ($user->getUsername() === 'admin')
            throw new AccessDeniedException('Access denied');

        $userManager = $this->container->get('fos_user.user_manager');
        $userManager->deleteUser($user);

        return true;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function checkUser(Request $request)
    {
	$contents = json_decode($request->getContent(), true);

        # Validate fields
        $constraint = new Assert\Collection([
            'email'     => new Assert\Email(),
            'password'  => new Assert\NotBlank(),
        ]);

        $contents = $this->validatorService->validate($contents, $constraint);
        $userManager = $this->container->get('fos_user.user_manager');
        $emailExist = $userManager->findUserByEmail($contents['email']);

         # Check if the user exists to prevent Integrity constraint violation error in the insertion
        if($emailExist) {
            $emailExist->setPlainPassword($contents['password']);
            $userManager->updateUser($emailExist);
            return true;
        }

        return false;
    }
}
