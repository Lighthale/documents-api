<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Directory;
use AppBundle\Entity\File;
use AppBundle\Service\AuditTrailService;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends FOSRestController
{
    /**
     * @param array $contents
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function handleResponse($contents = [])
    {
        $response['status']     = (key_exists('status', $contents)) ? $contents['status'] : true;
        $response['message']    = (key_exists('message', $contents)) ? $contents['message'] : 'success';

        if (key_exists('data', $contents))
            $response['data'] = $contents['data'];

        $view = $this->view($response);
        return $this->handleView($view);
    }

    /**
     * @Route(
     *     "/api/get-logs",
     *     name="get_logs",
     *     methods={"GET"}
     * )
     * @param AuditTrailService $auditTrailService
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\DBAL\DBALException
     */
    public function populateFilesAction(AuditTrailService $auditTrailService)
    {
        $auditTrails = $auditTrailService->getAuditTrailLogs(null,null,null);
        return $this->handleResponse(['data' => $auditTrails]);
    }
}