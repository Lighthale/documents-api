<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Directory;
use AppBundle\Entity\User;
use AppBundle\Service\UserManagerService;
use Symfony\Component\Debug\Exception\ContextErrorException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

class UserController extends ApiController
{
    /**
     * Get user details
     *
     * @Route(
     *     "/api/user/{id}",
     *     name="get_user_details",
     *     methods={"GET"}
     * )
     * @SWG\Tag(name="User")
     * @Security(name="Bearer")
     * @paramConverter ("user", class="AppBundle:User")
     * @param User|null $user
     * @param UserManagerService $userManagerService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getUserDetailsAction(User $user = null, UserManagerService $userManagerService)
    {
        $data = $userManagerService->getUserDetails($user);
        return $this->handleResponse(['data' => $data]);
    }

    /**
     * Update user details
     *
     * @Route(
     *     "/api/user/{id}",
     *     name="update_user_details",
     *     methods={"PUT"}
     * )
     * @SWG\Tag(name="User")
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="JSON Payload",
     *     required=true,
     *     format="application/json",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="email", type="string", example="john.doe@email.com"),
     *          @SWG\Property(property="password", type="string", example="yourpassword")
     *     )
     * )
     * @Security(name="Bearer")
     * @paramConverter ("user", class="AppBundle:User")
     * @param Request $request
     * @param User|null $user
     * @param UserManagerService $userManagerService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function putUpdateUserDetailsAction(Request $request, User $user = null, UserManagerService $userManagerService)
    {
        $data = $userManagerService->updateUser($request, $user);
        return $this->handleResponse(['data' => $data]);
    }

    /**
     * Delete a user
     *
     * @Route(
     *     "/api/user/{id}",
     *     name="delete_user_details",
     *     methods={"DELETE"}
     * )
     * @SWG\Tag(name="User")
     * @Security(name="Bearer")
     * @paramConverter ("user", class="AppBundle:User")
     * @param UserManagerService $userManagerService
     * @param User|null $user
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteUserAction(UserManagerService $userManagerService, User $user = null)
    {
        $userManagerService->deleteUser($user);
        return $this->handleResponse(['message' => 'User deleted.']);
    }

    /**
     * Get all users
     *
     * @Route(
     *     "/api/user",
     *     name="get_users",
     *     methods={"GET"}
     * )
     * @SWG\Tag(name="User")
     * @Security(name="Bearer")
     * @param UserManagerService $userManagerService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getUsersAction(UserManagerService $userManagerService)
    {
        $data = $userManagerService->getUsers();
        return $this->handleResponse(['data' => $data]);
    }

    /**
     * Add a user
     *
     * @Route(
     *     "/api/user",
     *     name="add_user",
     *     methods={"POST"}
     * )
     * @SWG\Tag(name="User")
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="JSON Payload",
     *     required=true,
     *     format="application/json",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="email", type="string", example="john.doe@email.com"),
     *          @SWG\Property(property="password", type="string", example="yourpassword")
     *     )
     * )
     * @Security(name="Bearer")
     * @param Request $request
     * @param UserManagerService $userManagerService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postUserAction(Request $request, UserManagerService $userManagerService)
    {
        $data = $userManagerService->addUser($request);
        return $this->handleResponse(['message' => 'User added', 'data' => $data]);
    }

    /**
     * Register user
     *
     * @Route(
     *     "/register",
     *     name="register_user",
     *     methods={"POST"}
     * )
     * @param Request $request
     * @param UserManagerService $userManagerService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function registerUserAction(Request $request, UserManagerService $userManagerService)
    {
        $data = $userManagerService->addUser($request);
        return $this->handleResponse(['message' => 'User added', 'data' => $data]);
    }

    /**
     * Check user
     *
     * @Route(
     *     "/check",
     *     name="check_user",
     *     methods={"POST"}
     * )
     * @param Request $request
     * @param UserManagerService $userManagerService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function checkUserAction(Request $request, UserManagerService $userManagerService)
    {
        $data = $userManagerService->checkUser($request);
        return $this->handleResponse(['data' => $data]);
    }
}
