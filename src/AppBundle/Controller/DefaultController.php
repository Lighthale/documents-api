<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Directory;
use AppBundle\Entity\File;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="welcome")
     */
    public function indexAction(Request $request)
    {
        return $this->json(['message' => 'Welcome to Document Management System API!']);
    }
}
