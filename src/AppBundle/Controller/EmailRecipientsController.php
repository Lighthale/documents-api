<?php

namespace AppBundle\Controller;

use AppBundle\Entity\EmailRecipients;
use AppBundle\Service\ValidatorService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints as Assert;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

class EmailRecipientsController extends ApiController
{
    /**
     * Get recipients for alerts
     *
     * @Route(
     *     "/api/alerts/recipients",
     *     name="get_recipients",
     *     methods={"GET"}
     * )
     * @SWG\Tag(name="Alerts")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getRecipientsAction()
    {
        $emailRepo = $this->getDoctrine()->getRepository('AppBundle:EmailRecipients');
        $emails = $emailRepo->findAll();

        return $this->handleResponse(['data' => $emails]);
    }

    /**
     * Add recipient
     *
     * @Route(
     *     "/api/alerts/recipients",
     *     name="add_recipient",
     *     methods={"POST"}
     * )
     * @SWG\Tag(name="Alerts")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function uploadFileAction(Request $request, ValidatorService $validatorService)
    {
        $contents = json_decode($request->getContent(), true);

        # Validate fields
        $constraint = new Assert\Collection([
            'email' => new Assert\Email()
        ]);
        $contents = $validatorService->validate($contents, $constraint);

        $em = $this->getDoctrine()->getManager();
        $recipient = new EmailRecipients();
        $recipient->setEmail($contents['email']);
        $em->persist($recipient);
        $em->flush();

        return $this->handleResponse(['data' => $recipient]);
    }

    /**
     * Delete recipient
     *
     * @Route(
     *     "/api/alerts/recipients/{id}",
     *     name="delete_recipient",
     *     methods={"DELETE"}
     * )
     * @SWG\Tag(name="Alerts")
     * @paramConverter ("recipient", class="AppBundle:EmailRecipients")
     * @param EmailRecipients $recipient
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteRecipientAction(EmailRecipients $recipient = null)
    {
        if (!$recipient)
            throw new NotFoundHttpException('Email not found.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($recipient);
        $em->flush();

        return $this->handleResponse(['message' => 'Recipient removed.']);
    }
}