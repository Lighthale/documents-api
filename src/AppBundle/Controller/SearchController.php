<?php

namespace AppBundle\Controller;

use AppBundle\Service\SearchService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

class SearchController extends ApiController
{

    protected $searchService;

    public function __construct(SearchService $searchService)
    {
        $this->searchService = $searchService;
    }

    /**
     * For search autocomplete feature
     *
     * @Route(
     *     "/api/search/autocomplete",
     *     name="search_autocomplete",
     *     methods={"GET"}
     * )
     * @SWG\Tag(name="Search")
     * @SWG\Parameter(
     *     name="keyword",
     *     in="query",
     *     type="string",
     *     description="The word you are searching"
     * )
     * @Security(name="Bearer")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function autoComplete(Request $request)
    {
        $result = $this->searchService->autoComplete($request);
        return $this->handleResponse(['data' => $result]);
    }

    /**
     * Get record
     *
     * @Route(
     *     "/api/search",
     *     name="search",
     *     methods={"GET"}
     * )
     * @SWG\Tag(name="Search")
     * @SWG\Parameter(
     *     name="keyword",
     *     in="query",
     *     type="string",
     *     description="The word you are searching"
     * )
     * @Security(name="Bearer")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getRecord(Request $request)
    {
        $result = $this->searchService->getRecordByParam($request);
        return $this->handleResponse(['data' => $result]);
    }
}
