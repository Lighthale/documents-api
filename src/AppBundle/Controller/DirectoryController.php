<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Directory;
use AppBundle\Service\DirectoryManagerService;
use AppBundle\Service\SerializerService;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

class DirectoryController extends ApiController
{
    /**
     * Get base directory
     *
     * @Route(
     *     "/api/directory",
     *     name="get_root_directory",
     *     methods={"GET"}
     * )
     * @SWG\Tag(name="Directory")
     * @Security(name="Bearer")
     * @param DirectoryManagerService $directoryManagerService
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getRootDirectoryAction(DirectoryManagerService $directoryManagerService, Request $request)
    {
        $data = $directoryManagerService->getRootDirectory($request);
        return $this->handleResponse(['data' => $data]);
    }

    /**
     * Get directory details by id
     *
     * @Route(
     *     name="get_directory",
     *     path="/api/directory/{id}",
     *     methods={"GET"}
     * )
     * @paramConverter ("directory", class="AppBundle:Directory")
     * @SWG\Tag(name="Directory")
     * @Security(name="Bearer")
     * @param DirectoryManagerService $directoryManagerService
     * @param Request $request
     * @param Directory|null $directory
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getDirectoryAction(DirectoryManagerService $directoryManagerService, Request $request, Directory $directory = null)
    {
        $data = $directoryManagerService->getDirectory($request, $directory);

        return $this->handleResponse(['data' => $data]);
    }

    /**
     * Add new directory
     *
     * @Route(
     *     "/api/directory",
     *     name="add_directory",
     *     methods={"POST"}
     * )
     * @SWG\Tag(name="Directory")
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="JSON Payload",
     *     required=true,
     *     format="application/json",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="parent_id", type="integer", example=0),
     *          @SWG\Property(property="name", type="string", example="New Folder")
     *     )
     * )
     * @Security(name="Bearer")
     * @param Request $request
     * @param DirectoryManagerService $directoryManagerService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postDirectoryAction(Request $request, DirectoryManagerService $directoryManagerService)
    {
        $data = $directoryManagerService->addNewDirectory($request);
        return $this->handleResponse(['data' => $data]);
    }

    /**
     * Rename a directory
     *
     * @Route(
     *     name="update_directory",
     *     path="/api/directory/{directoryId}",
     *     methods={"PUT"}
     * )
     * @SWG\Tag(name="Directory")
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="JSON Payload",
     *     required=true,
     *     format="application/json",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="name", type="string", example="New Folder")
     *     )
     * )
     * @Security(name="Bearer")
     * @param Request $request
     * @param DirectoryManagerService $directoryManagerService
     * @param $directoryId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function putDirectoryAction(Request $request, DirectoryManagerService $directoryManagerService, $directoryId)
    {
        $data = $directoryManagerService->updateDirectory($request, $directoryId);
        return $this->handleResponse(['data' => $data]);
    }

    /**
     * Delete one or more directory ids
     *
     * @Route(
     *     name="delete_directory",
     *     path="/api/directory",
     *     methods={"DELETE"}
     * )
     * @SWG\Tag(name="Directory")
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="JSON Payload",
     *     required=true,
     *     format="application/json",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *              property="ids",
     *              type="array",
     *              @SWG\Items("integer", example=1)
     *          )
     *     )
     * )
     * @Security(name="Bearer")
     * @param Request $request
     * @param DirectoryManagerService $directoryManagerService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteDirectoryAction(Request $request, DirectoryManagerService $directoryManagerService)
    {
        $directoryManagerService->deleteDirectories($request);

        return $this->handleResponse(['message' => 'Directory(ies) deleted.']);
    }

    /**
     * Get directory's activity
     *
     * @Route(
     *     name="get_directory_logs",
     *     path="/api/directory/{directoryId}/logs",
     *     methods={"GET"}
     * )
     * @SWG\Tag(name="Directory")
     * @Security(name="Bearer")
     * @param null $directoryId
     * @param DirectoryManagerService $directoryManagerService
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getDirectoryAuditTrailLogsAction(DirectoryManagerService $directoryManagerService, Request $request, $directoryId = null)
    {
        $data = $directoryManagerService->getDirectoryAuditTrailLogs($request, $directoryId);

        return $this->handleResponse(['data' => $data]);
    }
}