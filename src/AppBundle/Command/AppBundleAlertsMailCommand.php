<?php

namespace AppBundle\Command;

use AppBundle\Service\EmailAlertService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AppBundleAlertsMailCommand extends ContainerAwareCommand
{
    private $emailAlertService;

    public function __construct(EmailAlertService $emailAlertService, $name = null)
    {
        parent::__construct($name);
        $this->emailAlertService = $emailAlertService;
    }

    protected function configure()
    {
        $this
            ->setName('saffron-docs:send-expiring-doc-alerts')
            ->setDescription('Sends expiring documents email alerts')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $argument = $input->getArgument('argument');

        if ($input->getOption('option')) {
            // ...
        }

        $this->emailAlertService->sendExpiringDocsAlerts();
//        $output->writeln('Alerts sent!');
    }
}
