<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\OrderBy;

/**
 * Directory
 *
 * @ORM\Table(name="directories")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DirectoryRepository")
 */
class Directory extends BaseEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="directories")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Directory", inversedBy="child")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @var int
     *
     * @ORM\OneToMany(targetEntity="Directory", mappedBy="parent")
     * @OrderBy({"createdAt" = "DESC"})
     */
    private $child;

    /**
     * @var int
     *
     * @ORM\OneToMany(targetEntity="File", mappedBy="directory")
     * @OrderBy({"createdAt" = "DESC"})
     */
    private $files;

    public function __construct()
    {
        $this->child = new ArrayCollection();
        $this->files = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Directory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set userId
     *
     * @param User $user
     *
     * @return Directory
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get userId
     *
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set parentId
     *
     * @param Directory $parent
     *
     * @return Directory
     */
    public function setParent(Directory $parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return int
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return array
     */
    public function getChild()
    {
        return $this->child->toArray();
    }

    /**
     * @return array
     */
    public function getFiles()
    {
        return $this->files->toArray();
    }
}
