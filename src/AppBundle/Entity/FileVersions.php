<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FileVersions
 *
 * @ORM\Table(name="file_versions")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FileVersionsRepository")
 */
class FileVersions extends BaseEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="File")
     * @ORM\JoinColumn(name="file_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $file;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="server_file_name", type="string", length=255)
     */
    private $serverFileName;

    /**
     * @var string
     *
     * @ORM\Column(name="size", type="string", length=255)
     */
    private $size;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="uploaded_at", type="datetime")
     */
    private $uploadedAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param integer $file
     *
     * @return FileVersions
     */
    public function setFileId($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return int
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set serverFileName
     *
     * @param string $serverFileName
     *
     * @return FileVersions
     */
    public function setServerFileName($serverFileName)
    {
        $this->serverFileName = $serverFileName;

        return $this;
    }

    /**
     * Get serverFileName
     *
     * @return string
     */
    public function getServerFileName()
    {
        return $this->serverFileName;
    }

    /**
     * Set size
     *
     * @param string $size
     *
     * @return FileVersions
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return string
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set uploadedAt
     *
     * @param \DateTime $uploadedAt
     *
     * @return FileVersions
     */
    public function setUploadedAt($uploadedAt)
    {
        $this->uploadedAt = $uploadedAt;

        return $this;
    }

    /**
     * Get uploadedAt
     *
     * @return \DateTime
     */
    public function getUploadedAt()
    {
        return $this->uploadedAt->format('Y-m-d H:i:s');
    }

    /**
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param int $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }
}

