<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * File
 *
 * @ORM\Table(name="files")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FileRepository")
 */
class File extends BaseEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="server_file_name", type="string", length=255)
     */
    private $serverFileName;

    /**
     * @var string
     *
     * @ORM\Column(name="extension", type="string", length=255)
     */
    private $extension;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="size", type="integer")
     */
    private $size;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_modified", type="datetime", nullable=true)
     */
    private $dateModified;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="files")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Directory", inversedBy="files")
     * @ORM\JoinColumn(name="directory_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $directory;

    /**
     * @var int
     *
     * @ORM\OneToOne(targetEntity="FileMetadata", mappedBy="file")
     */
    private $metadata;

    /**
     * @var int
     *
     * @ORM\OneToMany(targetEntity="FileVersions", mappedBy="file")
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $versions;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param integer $user
     *
     * @return File
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set directory
     *
     * @param integer $directory
     *
     * @return File
     */
    public function setDirectory($directory)
    {
        $this->directory = $directory;

        return $this;
    }

    /**
     * Get directory
     *
     * @return int
     */
    public function getDirectory()
    {
        return $this->directory;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return File
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getServerFileName()
    {
        return $this->serverFileName;
    }

    /**
     * @param string $serverFileName
     */
    public function setServerFileName($serverFileName)
    {
        $this->serverFileName = $serverFileName;
    }

    /**
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @param string $extension
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param string $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return \DateTime|string
     */
    public function getDateModified()
    {
        if ($this->dateModified)
            return $this->dateModified->format('Y-m-d');
        else
            return $this->dateModified;
    }

    /**
     * @param \DateTime $dateModified
     */
    public function setDateModified($dateModified)
    {
        $this->dateModified = $dateModified;
    }

    /**
     * @return int
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param FileMetadata $metadata
     */
    public function setMetadata(FileMetadata $metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * @return int
     */
    public function getVersions()
    {
        return $this->versions;
    }

    /**
     * @param int $versions
     */
    public function setVersions($versions)
    {
        $this->versions = $versions;
    }
}

