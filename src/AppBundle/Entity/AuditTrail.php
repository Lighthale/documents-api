<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AuditTrail
 *
 * @ORM\Table(name="audit_trails")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AuditTrailRepository")
 */
class AuditTrail extends BaseEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="directories")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="string", length=255)
     */
    private $action;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Directory", inversedBy="directory")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $parent;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Directory", inversedBy="directory")
     * @ORM\JoinColumn(name="directory_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $directory;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="File", inversedBy="file")
     * @ORM\JoinColumn(name="file_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(name="remarks", type="string", length=255, nullable=true)
     */
    private $remarks;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param integer $user
     *
     * @return AuditTrail
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set action
     *
     * @param string $action
     *
     * @return AuditTrail
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @return int
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param int $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * Set directory
     *
     * @param integer $directory
     *
     * @return AuditTrail
     */
    public function setDirectory($directory)
    {
        $this->directory = $directory;

        return $this;
    }

    /**
     * Get directory
     *
     * @return int
     */
    public function getDirectory()
    {
        return $this->directory;
    }

    /**
     * Set file
     *
     * @param integer $file
     *
     * @return AuditTrail
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return int
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return string
     */
    public function getRemarks()
    {
        return $this->remarks;
    }

    /**
     * @param string $remarks
     */
    public function setRemarks($remarks)
    {
        $this->remarks = $remarks;
    }

    public function getCreatedAt()
    {
        return parent::getCreatedAt()->format('Y-m-d H:i:s');
    }
}

