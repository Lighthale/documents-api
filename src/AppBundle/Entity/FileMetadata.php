<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FileMetadata
 *
 * @ORM\Table(name="file_metadata")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FileMetadataRepository")
 */
class FileMetadata extends BaseEntity
{
    const STATUS_DRAFT = 'draft';
    const STATUS_ACTIVE = 'active';
    const STATUS_EXPIRED = 'expired';
    const ONE_DAY_BEFORE = 'one-day-before';
    const THREE_DAYS_BEFORE = 'three-days-before';
    const ONE_WEEK_BEFORE = 'one-week-before';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\OneToOne(targetEntity="File")
     * @ORM\JoinColumn(name="file_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="details", type="string", length=255, nullable=true)
     */
    private $details;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="doc_created_date", type="datetime", nullable=true)
     */
    private $docCreatedDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="valid_from", type="datetime", nullable=true)
     */
    private $validFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="valid_to", type="datetime")
     */
    private $validTo;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="version", type="string", length=255, nullable=true)
     */
    private $version;

    /**
     * @var bool
     *
     * @ORM\Column(name="alerts", type="boolean")
     */
    private $alerts = false;

    /**
     * @var string
     *
     * @ORM\Column(name="alert_on", type="string", length=255, nullable=true)
     */
    private $alertOn;

    /**
     * @var string
     *
     * @ORM\Column(name="author", type="string", length=255, nullable=true)
     */
    private $author;

    /**
     * @var string
     *
     * @ORM\Column(name="category", type="string", length=255, nullable=true)
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="confidential", type="string", length=255, nullable=true)
     */
    private $confidential;

    /**
     * @var string
     *
     * @ORM\Column(name="recipients", type="string", length=255, nullable=true)
     */
    private $recipients;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param integer $file
     *
     * @return FileMetadata
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return int
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return FileMetadata
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set details
     *
     * @param string $details
     *
     * @return FileMetadata
     */
    public function setDetails($details)
    {
        $this->details = $details;

        return $this;
    }

    /**
     * Get details
     *
     * @return string
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * Set docCreatedDate
     *
     * @param \DateTime $docCreatedDate
     *
     * @return FileMetadata
     */
    public function setDocCreatedDate($docCreatedDate = null)
    {
        if ($docCreatedDate)
            $this->docCreatedDate = $docCreatedDate;
        else
            $this->docCreatedDate = new \DateTime();

        return $this;
    }

    /**
     * Get docCreatedDate
     *
     * @return \DateTime
     */
    public function getDocCreatedDate()
    {
        if ($this->docCreatedDate)
            return $this->docCreatedDate->format('Y-m-d');
        else
            return $this->docCreatedDate;
    }

    /**
     * Set validFrom
     *
     * @param \DateTime $validFrom
     *
     * @return FileMetadata
     */
    public function setValidFrom($validFrom)
    {
        $this->validFrom = $validFrom;

        return $this;
    }

    /**
     * Get validFrom
     *
     * @return \DateTime
     */
    public function getValidFrom()
    {
        if ($this->validFrom)
            return $this->validFrom->format('Y-m-d');
        else
            return $this->validFrom;
    }

    /**
     * Set validTo
     *
     * @param \DateTime $validTo
     *
     * @return FileMetadata
     */
    public function setValidTo($validTo)
    {
        $this->validTo = $validTo;

        return $this;
    }

    /**
     * Get validTo
     *
     * @return \DateTime
     */
    public function getValidTo()
    {
        return $this->validTo->format('Y-m-d');
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return FileMetadata
     */
    public function setStatus($status = null)
    {
        if ($status)
            $this->status = $status;
        else
            $this->status = FileMetadata::STATUS_ACTIVE;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set version
     *
     * @param string $version
     *
     * @return FileMetadata
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set alerts
     *
     * @param boolean $alerts
     *
     * @return FileMetadata
     */
    public function setAlerts($alerts = true)
    {
        $this->alerts = $alerts;

        return $this;
    }

    /**
     * Get alerts
     *
     * @return bool
     */
    public function getAlerts()
    {
        return $this->alerts;
    }

    /**
     * @return string
     */
    public function getAlertOn()
    {
        return $this->alertOn;
    }

    /**
     * @param string $alertOn
     */
    public function setAlertOn($alertOn)
    {
        $this->alertOn = $alertOn;
    }

    /**
     * Set author
     *
     * @param string $author
     *
     * @return FileMetadata
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set category
     *
     * @param string $category
     *
     * @return FileMetadata
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set confidential
     *
     * @param string $confidential
     *
     * @return FileMetadata
     */
    public function setConfidential($confidential)
    {
        $this->confidential = $confidential;

        return $this;
    }

    /**
     * Get confidential
     *
     * @return string
     */
    public function getConfidential()
    {
        return $this->confidential;
    }

    /**
     * @return mixed|string
     */
    public function getRecipients()
    {
        if ($this->recipients)
            return json_decode($this->recipients);
        else
            return $this->recipients;
    }

    /**
     * @param string $recipients
     */
    public function setRecipients($recipients)
    {
        $this->recipients = $recipients;
    }
}

