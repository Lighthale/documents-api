<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181127025017 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('DROP VIEW IF EXISTS `document_view`;');
        $this->addSql('CREATE VIEW document_view as SELECT file.id as id, file.type as type, \'file\' as variant, file.name as name, file.directory_id as directory_id,  file.server_file_name as server_file_name,  file.user_id as user_id, user.username as username,  file.extension as extension, file.size as size, file.created_at as created_at, file.date_modified as date_modified FROM files as file LEFT JOIN users as user ON user.id = file.user_id
UNION
SELECT dir.id as id, \'directory\' as type, \'directory\' as variant,  dir.name as name, IFNULL(dir.parent_id, 0) as directory_id, null as server_file_name, dir.user_id as user_id, user.username as username, null as extension, null as size, dir.created_at as created_at, dir.updated_at as updated_at from directories as dir LEFT JOIN users as user ON user.id = dir.user_id');


    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('DROP VIEW IF EXISTS `document_view`;');
    }
}
