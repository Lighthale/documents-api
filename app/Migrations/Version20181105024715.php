<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181105024715 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('CREATE TABLE file_versions (id INT AUTO_INCREMENT NOT NULL, file_id INT DEFAULT NULL, user_id INT DEFAULT NULL, server_file_name VARCHAR(255) NOT NULL, size VARCHAR(255) NOT NULL, uploaded_at DATETIME NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_A88CCF4F93CB796C (file_id), INDEX IDX_A88CCF4FA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE file_versions ADD CONSTRAINT FK_A88CCF4F93CB796C FOREIGN KEY (file_id) REFERENCES files (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE file_versions ADD CONSTRAINT FK_A88CCF4FA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE files ADD date_modified DATETIME DEFAULT NULL AFTER size');
        $this->addSql('ALTER TABLE file_metadata ADD recipients VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE files CHANGE size size INT NOT NULL');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('DROP TABLE file_versions');
        $this->addSql('ALTER TABLE files DROP date_modified');
        $this->addSql('ALTER TABLE file_metadata DROP recipients');
        $this->addSql('ALTER TABLE files CHANGE size size VARCHAR(255) NOT NULL');
    }
}
