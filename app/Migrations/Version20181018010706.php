<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181018010706 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE files ADD server_file_name VARCHAR(255) NOT NULL AFTER name');
        $this->addSql('ALTER TABLE files ADD extension VARCHAR(255) NOT NULL AFTER server_file_name');
        $this->addSql('ALTER TABLE files ADD path VARCHAR(255) NOT NULL AFTER extension');
        $this->addSql('ALTER TABLE files ADD type VARCHAR(255) NOT NULL AFTER path');
        $this->addSql('ALTER TABLE files ADD size VARCHAR(255) NOT NULL AFTER type');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE files DROP server_file_name');
        $this->addSql('ALTER TABLE files DROP extension');
        $this->addSql('ALTER TABLE files DROP path');
        $this->addSql('ALTER TABLE files DROP type');
        $this->addSql('ALTER TABLE files DROP size');
    }
}
