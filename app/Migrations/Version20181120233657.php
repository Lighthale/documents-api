<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181120233657 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('CREATE TABLE audit_trails (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, parent_id INT DEFAULT NULL, directory_id INT DEFAULT NULL, file_id INT DEFAULT NULL, action VARCHAR(255) NOT NULL, remarks VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_456515E4A76ED395 (user_id), INDEX IDX_456515E4727ACA70 (parent_id), INDEX IDX_456515E42C94069F (directory_id), INDEX IDX_456515E493CB796C (file_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE audit_trails ADD CONSTRAINT FK_456515E4A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE audit_trails ADD CONSTRAINT FK_456515E4727ACA70 FOREIGN KEY (parent_id) REFERENCES directories (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE audit_trails ADD CONSTRAINT FK_456515E42C94069F FOREIGN KEY (directory_id) REFERENCES directories (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE audit_trails ADD CONSTRAINT FK_456515E493CB796C FOREIGN KEY (file_id) REFERENCES files (id) ON DELETE CASCADE');

        # Create view table
        $this->addSql('
            CREATE VIEW view_audit_trails AS
            SELECT a.id, u.username,
            pd.id AS parent_id, pd.name AS parent_name,
            d.id AS directory_id, d.name AS directory_name, 
            f.id AS file_id, CONCAT(f.name, ".", f.extension) AS file_name, f.`type` AS file_type,
            a.`action`, a.remarks, a.created_at
            FROM audit_trails a
            LEFT JOIN users u ON a.user_id = u.id
            LEFT JOIN directories pd ON a.parent_id = pd.id
            LEFT JOIN directories d ON a.directory_id = d.id
            LEFT JOIN files f ON a.file_id = f.id
        ');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('DROP VIEW view_audit_trails');
        $this->addSql('DROP TABLE audit_trails');
    }
}