<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181031063443 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE file_metadata CHANGE doc_created_date doc_created_date DATETIME DEFAULT NULL, CHANGE valid_from valid_from DATETIME DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE file_metadata ALTER valid_from DROP DEFAULT');
        $this->addSql('ALTER TABLE file_metadata ALTER doc_created_date DROP DEFAULT');
        $this->addSql('ALTER TABLE file_metadata CHANGE COLUMN valid_from valid_from DATETIME NOT NULL AFTER doc_created_date');
        $this->addSql('ALTER TABLE file_metadata CHANGE COLUMN doc_created_date doc_created_date DATETIME NOT NULL AFTER details');
    }
}
