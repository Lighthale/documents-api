install:
	php bin/console fos:oauth-server:create-client --grant-type=password --grant-type=refresh_token
	php bin/console fos:user:create admin admin qaz123wsx
	php bin/console fos:user:promote admin role_admin

setup:
	if [ -d var/cache ] ; then \
		mv var/cache var/cache-old && \
		rm -rf var/cache-old ; \
	fi
	mkdir var/cache 
	chmod -R 777 var/cache
	if [ -d var/logs ] ; then \
		mv var/logs var/logs-old && \
		rm -rf var/logs-old ; \
	fi
	mkdir var/logs 
	chmod -R 777 var/logs
	if [ -d var/sessions ] ; then \
		mv var/sessions var/sessions-old && \
		rm -rf var/sessions-old ; \
	fi
	mkdir var/sessions 
	chmod -R 777 var/sessions
	if [ ! -d uploads ] ; then \
		mkdir uploads ; \
	fi
	chmod -R 777 uploads
	composer install --no-interaction
	php bin/console doctrine:migrations:migrate --no-interaction

docker-logs-dev:
	rm -rf /code/var/logs/dev.log
	tail --retry --follow=name /code/var/logs/dev.log

docker-logs:
	rm -rf /code/var/logs/prod.log
	tail --retry --follow=name /code/var/logs/prod.log

app-install:
	docker-compose exec documents-api make install

app-setup:
	docker-compose exec documents-api make clean

app-bash:
	docker-compose exec documents-api bash
